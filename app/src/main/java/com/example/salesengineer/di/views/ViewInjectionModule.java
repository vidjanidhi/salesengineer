package com.example.salesengineer.di.views;

import android.view.View;

import java.util.Map;

import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.multibindings.Multibinds;

@Module
public abstract class ViewInjectionModule {
    @Multibinds
    abstract Map<Class<? extends View>, AndroidInjector.Factory<? extends View>> viewInjectorFactories();

    @Multibinds
    abstract Map<String, AndroidInjector.Factory<? extends View>> viewStringInjectorFactories();
}
