package com.example.customermanageadmi.application.location

import android.app.Application
import com.example.salesengineer.di.location.LocationManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocationManagerModule {
    @Provides
    @Singleton
    fun provideLocationManager(application: Application): LocationManager {
        return LocationManager(application)
    }
}