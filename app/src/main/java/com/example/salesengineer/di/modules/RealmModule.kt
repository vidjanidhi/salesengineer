package com.example.salesengineer.di.modules

import android.app.Application
import com.example.salesengineer.prefs.SharedPrefsService
import dagger.Module
import dagger.Provides
import io.realm.DynamicRealm
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmMigration
import javax.inject.Singleton


@Module
class RealmModule {
    @Provides
    @Singleton
    fun getRealm(realmConfiguration: RealmConfiguration): Realm {
        Realm.setDefaultConfiguration(realmConfiguration)
        return Realm.getDefaultInstance()
    }

    @Provides
    @Singleton
    fun provideRealmConfiguration(
        application: Application,
        deliverrSharedPrefsService: SharedPrefsService
    ): RealmConfiguration {
        Realm.init(application)
        return RealmConfiguration.Builder()
            .name("customer.realm")
            .schemaVersion(1)
            .migration(DeliverrMigration(deliverrSharedPrefsService))
            .build()
    }

    private inner class DeliverrMigration(private val deliverrSharedPrefsService: SharedPrefsService) :
        RealmMigration {

        override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
            var oldVersion = oldVersion
            val realmSchema = realm.schema
            if (oldVersion == 0L) {
            }
        }
    }

}
