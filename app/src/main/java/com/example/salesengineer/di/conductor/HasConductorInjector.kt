package com.example.salesengineer.di.conductor

import com.bluelinelabs.conductor.Controller
import dagger.android.DispatchingAndroidInjector

interface HasConductorInjector {
    fun controllerInjector(): DispatchingAndroidInjector<Controller>
}