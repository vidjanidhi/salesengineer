package com.example.salesengineer.di.views

import android.view.View
import dagger.android.AndroidInjector

interface HasViewInjector {
    fun viewInjector(): AndroidInjector<View>
}