package com.example.salesengineer.di.conductor;

import com.bluelinelabs.conductor.Controller;

import java.util.Map;

import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.multibindings.Multibinds;

/**
 * Conductor injection module
 * <p>
 * Note: the use of java here due to Kotlin's weird handling of generics
 */
@Module
public abstract class ConductorInjectionModule {

    @Multibinds
    abstract Map<Class<? extends Controller>, AndroidInjector.Factory<?>> controllerInjectorFactories();

    @Multibinds
    abstract Map<String, AndroidInjector.Factory<? extends Controller>> controllerStringInjectorFactories();
}
