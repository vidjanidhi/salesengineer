package com.example.salesengineer.di.components

import android.app.Application
import com.example.salesengineer.application.MyApplication
import com.example.salesengineer.di.conductor.ConductorInjectionModule
import com.example.salesengineer.di.modules.*
import com.example.salesengineer.di.views.ViewInjectionModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        SharedPrefsModule::class,
        AndroidSupportInjectionModule::class,
        ConductorInjectionModule::class,
        ViewInjectionModule::class,
        RealmModule::class,
        ActivityBindingModule::class,
        ConductorBindingModule::class,
        ViewBindingModule::class
    ]
)
interface DeliverrApplicationComponent : AndroidInjector<MyApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): DeliverrApplicationComponent
    }
}
