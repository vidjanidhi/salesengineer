package com.example.salesengineer.di.modules

import android.app.Application
import com.example.salesengineer.prefs.SharedPrefs
import com.example.salesengineer.prefs.SharedPrefsService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SharedPrefsModule {
    @Provides
    @Singleton
    fun provideDeliverrSharedPrefsService(application: Application): SharedPrefsService {
        return SharedPrefs(application)
    }
}