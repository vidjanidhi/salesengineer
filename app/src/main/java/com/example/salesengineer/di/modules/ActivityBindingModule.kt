package com.example.salesengineer.di.modules

import com.example.salesengineer.main.splash.SplashActivity
import com.example.salesengineer.main.customers.CustomersListActivity
import com.example.salesengineer.main.customers.livedata.CustomerListActivityModule
import com.example.salesengineer.main.home.MainActivity
import com.example.salesengineer.main.home.livedata.MainActivityModule
import com.example.salesengineer.main.login.LoginActivity
import com.example.salesengineer.main.logout.LogoutActivit
import com.example.salesengineer.main.register.AddUserActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import deliverr.deliverrconsumer.di.scopes.ActivityScope


@Module
abstract class ActivityBindingModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindHomeActivity(): MainActivityModule

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindSplashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindAddEngineerActivity(): AddUserActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [CustomerListActivityModule::class])
    internal abstract fun bindSBrandItemsActivity(): CustomersListActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindLogoutActivity(): LogoutActivit

}
