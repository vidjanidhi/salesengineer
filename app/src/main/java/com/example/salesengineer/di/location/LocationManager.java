package com.example.salesengineer.di.location;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.salesengineer.application.MyApplication;
import com.example.salesengineer.application.utils.PermissionUtils;
import com.example.salesengineer.models.LocationHistory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class LocationManager implements
        LocationListener,
        ConnectionCallbacks,
        OnConnectionFailedListener {
    public static final Location UNAVAILABLE_LOCATION = new Location("unavailable");

    // Update interval in milliseconds
    private static final long UPDATE_INTERVAL_MILIS = TimeUnit.SECONDS.toMillis(5);

    // A fast ceiling of update intervals, used when the app is visible
    private static final long FAST_INTERVAL_CEILING_MILIS = TimeUnit.SECONDS.toMillis(1);

    // onStop, disconnect the client if this time elapses without another connect()
    private static final long DISCONNECT_TIMER_MILIS = TimeUnit.SECONDS.toMillis(4);

    // If a cached location is older than this, discard it
    private static final long CACHE_TIMEOUT_MILIS = TimeUnit.MINUTES.toMillis(10);

    // Time that the location history will keep cached locations.
    private static final long LOCATIONS_HISTORY_CACHED_TIME = TimeUnit.MINUTES.toMillis(2);
    private static final String TAG = LocationManager.class.getCanonicalName();

    // TODO: could consider storing this in sharedprefs
    private static Location cachedLocation;
    private static boolean locationServicesEnabled = true;

    private Context context;
    private Handler handler;
    private OnConnectionFailedListener connectionFailedListener;

    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;

    private List<LocationHistory> locationsHistory = new ArrayList<>();
    private List<LocationListener> locationListeners = new ArrayList<>();

    /**
     * Hide default constructor
     */
    private LocationManager() {
    }

    /**
     * Creates a location manager and registers it with the application's ResumeFromBackgroundLifecycleHandler callback listener.
     *
     * @param context
     */
    public LocationManager(Context context) {
        this.context = context.getApplicationContext();

        handler = new Handler();

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(UPDATE_INTERVAL_MILIS);
        locationRequest.setFastestInterval(FAST_INTERVAL_CEILING_MILIS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Register a location listener. Should be done onStart.
     * Don't forget to un-register onStop.
     */
    public void registerLocationListener(LocationListener listener) {
        synchronized (locationListeners) {
            if (!locationListeners.contains(listener)) {
                locationListeners.add(listener);
                listener.onLocationChanged(getLastLocation());
            }
        }
    }

    public void unregisterLocationListener(LocationListener listener) {
        synchronized (locationListeners) {
            locationListeners.remove(listener);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionFailedListener != null) {
            connectionFailedListener.onConnectionFailed(connectionResult);
        }
    }

    private void startLocationUpdates() {
        if (googleApiClient.isConnected()) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            } catch (SecurityException e) {
                //Ignore exception as the updates will occur when
                //we have location permission
            }

        } else {
            googleApiClient.connect(); // location updates will be requested after the client is connected
        }
    }

    private void stopLocationUpdates() {
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
    }

    public void connectClient(OnConnectionFailedListener connectionFailedListener) {
        handler.removeCallbacks(disconnectTimer);
        this.connectionFailedListener = connectionFailedListener;

        if (!googleApiClient.isConnected()) {
            googleApiClient.connect();
        } else {
            // Already connected, replace the existing location update request with a new one, if
            // it is even still there
            startLocationUpdates();
        }

        // update location services enabled flag every connect
        locationServicesEnabled = areLocationServicesEnabled(context);
    }

    public boolean isGoogleApiClientConnected() {
        if (googleApiClient != null) {
            return googleApiClient.isConnected();
        } else {
            return false;
        }
    }

    public void disconnectClient() {
        Log.d(MyApplication.TAG, "LocationManager: disconnect");

        handler.removeCallbacks(disconnectTimer);
        connectionFailedListener = null;

        stopLocationUpdates();
    }

    public void startDisconnectTimer() {
        handler.postDelayed(disconnectTimer, DISCONNECT_TIMER_MILIS);
    }

    /**
     * @return the last good location or the "unavailable" location if no last good location exists.  Never returns null.
     */
    @NonNull
    public Location getLastLocation() {
        // return empty location if location services are disabled
        if (!areLocationServicesEnabled(context)) {
            return UNAVAILABLE_LOCATION;
        }

        try {
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {
                // update the cache with the fresh location from the client
                cachedLocation = location;
                return cachedLocation;
            } else {
                return isCachedLocationValid() ? cachedLocation : UNAVAILABLE_LOCATION;
            }
        } catch (SecurityException e) {
            return UNAVAILABLE_LOCATION;
        }
    }

    public void showEnableLocationDialog(final Activity activity, final int requestCode) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
//        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. We do not need to do anything here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied but could be fixed by showing the user a dialog.
                        try {
                            status.startResolutionForResult(activity, requestCode);
                        } catch (IntentSender.SendIntentException e) {
                            PermissionUtils.openSystemSettingsLocationScreen(activity);
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        PermissionUtils.openSystemSettingsLocationScreen(activity);
                        break;
                }
            }
        });
    }

    private boolean isCachedLocationValid() {
        if (cachedLocation == null) {
            return false;
        }

        return System.currentTimeMillis() - cachedLocation.getTime() < CACHE_TIMEOUT_MILIS;
    }

    /**
     * Checks if the location service is enabled by retrieving a list of all
     * providers.
     */
    public static boolean areLocationServicesEnabled(Context context) {
        String locationsAllowed = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        return !TextUtils.isEmpty(locationsAllowed);
    }

    @Override
    public void onLocationChanged(Location location) {
        // update the cache with fresh location
        cachedLocation = location;
        insertLocationHistory(location);
        removeExpiredLocationHistory();

        // notify listeners
        synchronized (locationListeners) {
            for (LocationListener listener : locationListeners) {
                listener.onLocationChanged(location);
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }

    Runnable disconnectTimer = new Runnable() {
        @Override
        public void run() {
            disconnectClient();
        }
    };

    @Override
    public void onConnectionSuspended(int arg0) {
    }

    private void insertLocationHistory(Location latestLocation) {
        Date now = new Date();
        LocationHistory locationHistory = new LocationHistory(latestLocation, now);
        synchronized (locationsHistory) {
            locationsHistory.add(0, locationHistory);
        }
    }

    private void removeExpiredLocationHistory() {
        Date now = new Date();

        synchronized (locationsHistory) {
            int i = locationsHistory.size() - 1;
            while (now.getTime() - locationsHistory.get(i).getDate().getTime() > LOCATIONS_HISTORY_CACHED_TIME) {
                locationsHistory.remove(i);
                i--;
            }
        }
    }

    /**
     * Returns the speed of the last known location
     *
     * @return the speed of the last known location
     */
    public float getSpeed() {
        return getLastLocation().getSpeed();
    }

    /**
     * A blocking method that awaits a GPS location before returning.
     * Checks every 100ms to see if we have a valid location
     *
     * @param timeout
     */
    public void awaitCloseLocation(final long timeout) {
        // Guard Clause
        if (getLastLocation().hasAccuracy()) {
            return;
        }

        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                long timeElapsed = 0;

                while (timeElapsed < timeout) {
                    try {
                        Thread.sleep(100);
                        timeElapsed += 100;
                    } catch (InterruptedException e) {
                        // Thread was interrupted, fast forward elapsed time to timeout
                        timeElapsed = timeout;
                    }
                }
            }
        });

        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (getLastLocation().hasAccuracy()) {
                    thread.interrupt();
                }
            }
        };

        registerLocationListener(locationListener);

        try {
            thread.start();
            thread.join();
        } catch (InterruptedException ignored) {

        }

        unregisterLocationListener(locationListener);
    }
}
