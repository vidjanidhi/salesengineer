package com.example.salesengineer.prefs

import android.content.Context
import android.content.SharedPreferences

class SharedPrefs(context: Context) : SharedPrefsService {
    private val sharedPreferences: SharedPreferences

    init {
        this.sharedPreferences = context.getSharedPreferences(DELIVERR_PREFS, Context.MODE_PRIVATE)
    }

    override var isLogin: Boolean
        get() = sharedPreferences.getBoolean(IS_LOGIN, false)
        set(b) = this.sharedPreferences.edit()
            .putBoolean(IS_LOGIN, b)
            .apply()

    override var engineerName: String
        get() = sharedPreferences.getString(ADMIN_NAME, "")
        set(b) = this.sharedPreferences.edit()
            .putString(ADMIN_NAME, b)
            .apply()

    override var engineerId: String
        get() = sharedPreferences.getString(ADMIN_ID, "")
        set(b) = this.sharedPreferences.edit()
            .putString(ADMIN_ID, b)
            .apply()

    override var isExternalStoragePermissionAsked: Boolean
        get() = sharedPreferences.getBoolean(IS_STORAGE_PERMISSION_ASKED, DEFAULT_BOOLEAN)
        set(b) = this.sharedPreferences.edit()
            .putBoolean(IS_STORAGE_PERMISSION_ASKED, b)
            .apply()


    override var isCameraPermissionAsked: Boolean
        get() = sharedPreferences.getBoolean(IS_CAMERA_PERMISSION_ASKED, DEFAULT_BOOLEAN)
        set(b) = this.sharedPreferences.edit()
            .putBoolean(IS_CAMERA_PERMISSION_ASKED, b)
            .apply()


    override var isLocationPermissionAsked: Boolean
        get() = sharedPreferences.getBoolean(IS_LOCATION_PERMISSION_ASKED, DEFAULT_BOOLEAN)
        set(b) = this.sharedPreferences.edit()
            .putBoolean(IS_LOCATION_PERMISSION_ASKED, b)
            .apply()


    override fun shouldReinit(): Boolean {
        return sharedPreferences.getBoolean(IS_REINIT, false)
    }

    override fun setIsAlcoholAgreed(value: Boolean) {
        this.sharedPreferences.edit()
            .putBoolean(LIQUOR_CONSENT, value)
            .apply()
    }
    override fun clearPreferences(): Boolean {
        try {
            return sharedPreferences.edit().clear().commit()
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }

    }


    companion object {
        private val DELIVERR_PREFS = "DELIVERR_PREFS"
        private val DEFAULT_BOOLEAN = false
        private val IS_LOGIN = "IS_LOGIN"
        private val ADMIN_NAME = "ADMIN_NAME"
        private val ADMIN_ID = "ADMIN_ID"
        private val IS_STORAGE_PERMISSION_ASKED = "IS_STORAGE_PERMISSION_ASKED"
        private val IS_CAMERA_PERMISSION_ASKED = "IS_CAMERA_PERMISSION_ASKED"
        private val IS_LOCATION_PERMISSION_ASKED = "IS_LOCATION_PERMISSION_ASKED"
        private val IS_REINIT = "IS_REINIT"
        private val LIQUOR_CONSENT = "LIQUOR_CONSENT"
    }
}
