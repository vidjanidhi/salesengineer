package com.example.salesengineer.prefs

interface SharedPrefsService {
    var isLogin: Boolean

    var engineerName: String
    var engineerId: String

    var isExternalStoragePermissionAsked: Boolean

    var isCameraPermissionAsked: Boolean

    var isLocationPermissionAsked: Boolean

    fun shouldReinit(): Boolean

    fun setIsAlcoholAgreed(value: Boolean)
    fun clearPreferences(): Boolean

}
