package com.example.customermanageadmin.realm_model

import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.home.livedata.FilterData
import io.realm.Case
import io.realm.Realm
import io.realm.RealmList
import javax.inject.Inject

class CustomerRealmRepository : CustomerRepository {
    lateinit var realm: Realm

    @Inject
    constructor(realm: Realm) {
        this.realm = realm
    }

    constructor() {}

    override fun getUserInfo(): Customer? {
        initRealm()
        return realm.where(Customer::class.java).findFirst()
    }

    override fun closeRealm() {
        if (realm != null && !realm.isClosed) {
            realm.close()
        }
    }

    private fun initRealm() {
        if (realm == null || realm.isClosed) {
            realm = Realm.getDefaultInstance()
        }
    }

    override fun insertItem(savedItem: Customer, id: String) {
        initRealm()
        realm.executeTransaction { realm ->
            savedItem.setItemId(id)
            realm.insert(savedItem)
        }
    }

    override fun insertOrUpdate(savedItem: List<Customer>) {
        initRealm()
        val customerList = RealmList<Customer>()
        customerList.addAll(savedItem)
        realm.executeTransaction { realm -> realm.insertOrUpdate(customerList) }
    }

    override fun insertOrUpdate(savedItem: Customer) {
        initRealm()
        realm.executeTransaction { realm -> realm.insertOrUpdate(savedItem) }
    }

    override fun countTotalItems(): Int {
        initRealm()
        return realm.where(Customer::class.java).count().toInt()
    }

    override fun countTotalAmount(): Float {
        initRealm()
        return realm.where(Customer::class.java).sum("amount").toFloat()
    }

    override fun countTotalItemsFilter(filterData: FilterData): Int {
        initRealm()
        return if (filterData.from_date != "" && filterData.paymentMode != 0) {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())

                .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThan("created", filterData.toDate)

                .findAll().count().toInt()
        } else if (filterData.paymentMode != 0 && filterData.from_date == "") {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())
                .findAll().count().toInt()

        } else if (filterData.paymentMode == 0 && filterData.from_date == "") {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll().count().toInt()

        } else if (filterData.paymentMode == 0 && filterData.from_date != "") {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll()
                .where()

                .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThan("created", filterData.toDate)

                .findAll().count().toInt()
        } else {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll().count().toInt()
        }
//        return realm.where(Customer::class.java).count().toInt()
    }

    override fun countTotalAmountFilter(filterData: FilterData): Float {
        initRealm()
        return if (filterData.from_date != "" && filterData.paymentMode != 0) {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())

                .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThan("created", filterData.toDate)

                .findAll().sum("amount").toFloat()
        } else if (filterData.paymentMode != 0 && filterData.from_date == "") {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())
                .findAll().sum("amount").toFloat()

        } else if (filterData.paymentMode == 0 && filterData.from_date == "") {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll().sum("amount").toFloat()

        } else if (filterData.paymentMode == 0 && filterData.from_date != "") {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll()
                .where()

                .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThan("created", filterData.toDate)

                .findAll().sum("amount").toFloat()
        } else {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll().sum("amount").toFloat()
        }


//        return realm.where(Customer::class.java).sum("amount").toFloat()
    }

    override fun findAll(): List<Customer> {
        initRealm()
        return realm.where(Customer::class.java)
            .findAll()
    }

    override fun findWithFilter(filterData: FilterData): List<Customer> {
        initRealm()
        return if (filterData.from_date != "" && filterData.paymentMode != 0) {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())

                .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThanOrEqualTo("created", filterData.toDate)

                .findAll()
        } else if (filterData.paymentMode != 0 && filterData.from_date == "") {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())

                .findAll()

        } else if (filterData.paymentMode == 0 && filterData.from_date == "") {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()

        } else if (filterData.paymentMode == 0 && filterData.from_date != "") {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .between("created", filterData.fromDate, filterData.toDate)

               /* .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThanOrEqualTo("created", filterData.toDate)*/

                .findAll()
        } else {
            realm.where(Customer::class.java).findAll().where()
                .equalTo("isAdd", false)
                .equalTo("isEdit", false)
                .findAll()
                .where()


                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
        }


    }

    override fun deleteAllItems() {
        initRealm()
        realm.executeTransaction { realm -> realm.delete(Customer::class.java) }
    }

    override fun deleteEditedItem(id: String) {
        initRealm()
        realm.executeTransaction { realm ->
            val rows = realm.where(Customer::class.java!!)
                .equalTo("isEdit", true).and()
                .equalTo("itemId", id)
                .findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()
    }

    override fun deleteAddedItem(email: String) {
        initRealm()
        realm.executeTransaction { realm ->
            val rows = realm.where(Customer::class.java!!)
                .equalTo("isAdd", true).and()
                .equalTo("email", email)
                .findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()
    }

    override fun deleteAllGetItems() {
        initRealm()
        realm.executeTransaction { realm ->
            val rows = realm.where(Customer::class.java!!)
                .equalTo("isEdit", false).and()
                .equalTo("isAdd", false)
                .findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()
    }

    override fun insertAddedItem(savedItem: Customer, id: String) {
        initRealm()
        realm.executeTransaction { realm ->
            savedItem.setItemId(id)
            realm.insert(savedItem)
        }
        closeRealm()

        initRealm()
        realm.executeTransaction { realm ->
            savedItem.setItemId(id)
            savedItem.edit = false
            savedItem.add = false
            realm.insert(savedItem)
        }
        closeRealm()
    }

    override fun insertEditedItem(savedItem: Customer, id: String) {
        initRealm()
        realm.executeTransaction { realm ->
            val rows = realm.where(Customer::class.java!!)
                .equalTo("isEdit", false).and()
                .equalTo("isAdd", false).and()
                .equalTo("itemId", savedItem.itemId.toString())
                .findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()

        initRealm()
        realm.executeTransaction { realm ->
            savedItem.setItemId(id)
            realm.insert(savedItem)
        }
        closeRealm()

        initRealm()
        realm.executeTransaction { realm ->
            savedItem.setItemId(id)
            savedItem.edit = false
            savedItem.add = false
            realm.insert(savedItem)
        }
        closeRealm()
    }

    override fun findEdits(): List<Customer> {
        initRealm()
        return realm.where(Customer::class.java)
            .equalTo("isEdit", true)
            .findAll()
    }

    override fun findAdds(): List<Customer> {
        initRealm()
        return realm.where(Customer::class.java)
            .equalTo("isAdd", true)
            .findAll()
    }

    override fun deleteEditedItems() {
        realm.executeTransaction { realm ->
            val rows = realm.where(Customer::class.java!!)
                .equalTo("isEdit", true).and()
                .findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()
    }

    override fun deleteAddedItems() {
        realm.executeTransaction { realm ->
            val rows = realm.where(Customer::class.java!!).equalTo("isAdd", true).findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()
    }


}
