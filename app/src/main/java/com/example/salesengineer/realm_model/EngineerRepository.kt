package com.example.customermanageadmin.realm_model

import com.example.salesengineer.main.customers.Engineer
import java.util.logging.Filter

interface EngineerRepository {
    fun getUserInfo(): Engineer?

    fun closeRealm()

    fun insertItem(savedItem: Engineer, id: String)

    fun insertOrUpdate(savedItem: List<Engineer>)

    fun insertOrUpdate(savedItem: Engineer)

    fun findAll(): List<Engineer>

    fun deleteAllItems()

    fun countTotalItems(): Int

    fun findEdits(): List<Engineer>

    fun findAdds(): List<Engineer>

    fun deleteEditedItems()
    fun deleteEditedItem(id:String)

    fun deleteAddedItems()
    fun deleteAddedItem(email:String)

    fun deleteAllGetItems()
    fun insertAddedItem(savedItem: Engineer, id: String)
    fun insertEditedItem(savedItem: Engineer, id: String)
}
