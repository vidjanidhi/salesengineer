package com.example.customermanageadmin.realm_model

import com.example.salesengineer.main.customers.Engineer
import io.realm.Realm
import io.realm.RealmList
import javax.inject.Inject


class EngineerRealmRepository : EngineerRepository {
    /*override fun insertOrUpdate(savedItem: Engineer) {
        initRealm()
        realm.executeTransaction { realm ->
            val rows = realm.where(Engineer::class.java!!)
                .equalTo("isEdit", false).and()
                .equalTo("isAdd", false).and()
                .equalTo("id", savedItem.id)
                .findAll()
            rows.deleteAllFromRealm()
            insertItem(savedItem, savedItem.id)
            closeRealm()
        }
    }*/

    override fun deleteEditedItem(id: String) {
        initRealm()
        realm.executeTransaction { realm ->
            val rows = realm.where(Engineer::class.java!!)
                .equalTo("isEdit", true).and()
                .equalTo("id", id)
                .findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()
    }

    override fun deleteAddedItem(email: String) {
        initRealm()
        realm.executeTransaction { realm ->
            val rows = realm.where(Engineer::class.java!!)
                .equalTo("isAdd", true).and()
                .equalTo("email", email)
                .findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()
    }

    override fun deleteAllGetItems() {
        initRealm()
        realm.executeTransaction { realm ->
            val rows = realm.where(Engineer::class.java!!)
                .equalTo("isEdit", false).and()
                .equalTo("isAdd", false)
                .findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()
    }

    lateinit var realm: Realm

    @Inject
    constructor(realm: Realm) {
        this.realm = realm
    }

    constructor() {}

    override fun getUserInfo(): Engineer? {
        initRealm()
        return realm.where(Engineer::class.java).findFirst()
    }

    override fun closeRealm() {
        if (realm != null && !realm.isClosed) {
            realm.close()
        }
    }

    private fun initRealm() {
        if (realm == null || realm.isClosed) {
            realm = Realm.getDefaultInstance()
        }
    }

    override fun insertItem(savedItem: Engineer, id: String) {
        initRealm()
        realm.executeTransaction { realm ->
            savedItem.id = id
            realm.insert(savedItem)
        }
        closeRealm()
    }

    override fun insertAddedItem(savedItem: Engineer, id: String) {
        initRealm()
        realm.executeTransaction { realm ->
            savedItem.id = id
            realm.insert(savedItem)
        }
        closeRealm()
        initRealm()
        realm.executeTransaction { realm ->
            savedItem.id = id
            savedItem.edit = false
            savedItem.add = false
            realm.insert(savedItem)
        }
        closeRealm()
    }

    override fun insertEditedItem(savedItem: Engineer, id: String) {
        initRealm()
        realm.executeTransaction { realm ->
            val rows = realm.where(Engineer::class.java!!)
                .equalTo("isEdit", false).and()
                .equalTo("isAdd", false).and()
                .equalTo("id", savedItem.id)
                .findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()
        initRealm()
        realm.executeTransaction { realm ->
            savedItem.id = id
            realm.insert(savedItem)
        }
        closeRealm()
        initRealm()
        realm.executeTransaction { realm ->
            savedItem.id = id
            savedItem.edit = false
            savedItem.add = false
            realm.insert(savedItem)
        }
        closeRealm()
    }

    override fun insertOrUpdate(savedItem: List<Engineer>) {
        initRealm()
        val EngineerList = RealmList<Engineer>()
        EngineerList.addAll(savedItem)
        realm.executeTransaction { realm -> realm.insertOrUpdate(EngineerList) }
        closeRealm()
    }

    override fun insertOrUpdate(savedItem: Engineer) {
        initRealm()
        realm.executeTransaction { realm -> realm.insertOrUpdate(savedItem) }
    }


    override fun findAll(): List<Engineer> {
        initRealm()
        return realm.where(Engineer::class.java)
            .equalTo("isEdit", false)
            .equalTo("isAdd", false)
            .findAll()
    }

    override fun findEdits(): List<Engineer> {
        initRealm()
        return realm.where(Engineer::class.java)
            .equalTo("isEdit", true)
            .findAll()
    }

    override fun findAdds(): List<Engineer> {
        initRealm()
        return realm.where(Engineer::class.java)
            .equalTo("isAdd", true)
            .findAll()
    }

    override fun countTotalItems(): Int {
        initRealm()
        return realm.where(Engineer::class.java).count().toInt()
    }

    override fun deleteAllItems() {
        initRealm()
        realm.executeTransaction { realm -> realm.delete(Engineer::class.java) }
        closeRealm()
    }

    override fun deleteEditedItems() {
        realm.executeTransaction { realm ->
            val rows = realm.where(Engineer::class.java!!)
                .equalTo("isEdit", true).and()
                .findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()
    }

    override fun deleteAddedItems() {
        realm.executeTransaction { realm ->
            val rows = realm.where(Engineer::class.java!!).equalTo("isAdd", true).findAll()
            rows.deleteAllFromRealm()
        }
        closeRealm()
    }
}
