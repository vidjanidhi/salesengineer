package com.example.customermanageadmin.realm_model

import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.home.livedata.FilterData
import java.util.logging.Filter

interface CustomerRepository {
    fun getUserInfo(): Customer?

    fun closeRealm()

    fun insertItem(savedItem: Customer, id: String)

    fun insertOrUpdate(savedItem: List<Customer>)

    fun insertOrUpdate(savedItem: Customer)

    fun findAll(): List<Customer>
    fun findWithFilter(filterData: FilterData): List<Customer>

    fun countTotalItems(): Int
    fun countTotalAmount(): Float

    fun deleteAllItems()
    fun countTotalItemsFilter(filterData: FilterData): Int
   fun countTotalAmountFilter(filterData: FilterData): Float

    fun findEdits(): List<Customer>

    fun findAdds(): List<Customer>

    fun deleteEditedItems()
    fun deleteEditedItem(id:String)

    fun deleteAddedItems()
    fun deleteAddedItem(email:String)

    fun deleteAllGetItems()
    fun insertAddedItem(savedItem: Customer, id: String)
    fun insertEditedItem(savedItem: Customer, id: String)
}
