package com.example.customermanageadmin.realm_model

import com.example.salesengineer.main.customers.Engineer
import com.example.salesengineer.main.customers.livedata.Customer

class RealmManager(private val engineerRepository: CustomerRepository) {

    fun checkIfNeedsSync(): Boolean {
        var list: List<Customer> = engineerRepository.findEdits()
        var list1: List<Customer> = engineerRepository.findAdds()

        return if (list.size > 0 || list1.size > 0) {
            true
        } else {
            false
        }
    }


}