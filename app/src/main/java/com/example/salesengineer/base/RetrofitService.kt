package com.example.salesengineer.base

import com.example.salesengineer.main.Constants
import com.example.salesengineer.main.customers.CustomerAnalyticResponse
import com.example.salesengineer.main.customers.livedata.CustomerListResponse
import com.example.salesengineer.main.login.rx.AdminLoginResponse
import com.example.salesengineer.main.register.rx.AddEngineerResponse
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

internal interface RetrofitService {
    @GET(Constants.Admin.LOGIN)
    fun adminLogin(
        @Query("email") email: String,
        @Query("password") password: String
    ): Single<AdminLoginResponse>

    @GET(Constants.Admin.ADD_USER)
    fun addUser(
        @Query("name") name: String,
        @Query("email") email: String,
        @Query("phone") phone: String,
        @Query("amount") amount: String,
        @Query("eng_id") eng_id: String,
        @Query("payment_mode") payment_mode: Int,
        @Query("lat") lat: String,
        @Query("lng") lng: String
    ): Single<AddEngineerResponse>

    @GET(Constants.Admin.EDIT_USER)
    fun editUser(
        @Query("id") id: String,
        @Query("name") name: String,
        @Query("email") email: String,
        @Query("phone") phone: String,
        @Query("amount") amount: String,
        @Query("eng_id") eng_id: String,
        @Query("payment_mode") payment_mode: Int
    ): Single<AddEngineerResponse>

    @GET(Constants.Admin.GET_CUSTOMERS)
    fun getBrandItemList(
        @Query("eng_id") engineerId: String,
        @Query("ul") skip: Int,
        @Query("ll") take: Int
    ): Call<CustomerListResponse>

    @GET(Constants.Admin.GET_CUSTOMERS)
    fun getBrandItemList(
        @Query("name") name: String,
        @Query("time") time: String,
        @Query("from_date") from_date: String,
        @Query("to_date") to_date: String,
        @Query("eng_id") engineerId: String,
        @Query("pay_mode") payment_mode: String,
        @Query("ul") skip: Int,
        @Query("ll") take: Int
    ): Call<CustomerListResponse>

    @GET(Constants.Admin.GET_CUSTOMERS)
    fun getBrandItemList(
        @Query("eng_id") engineerId: String
    ): Single<CustomerListResponse>

    @GET(Constants.Admin.GET_CUSTOMERS_DETAIL)
    fun getBrandItemListDetail(
        @Query("eng_id") engineerId: String
    ): Single<CustomerAnalyticResponse>

    @GET(Constants.Admin.GET_CUSTOMERS_DETAIL)
    fun getBrandItemListDetail(
        @Query("name") name: String,
        @Query("time") time: String,
        @Query("from_date") from_date: String,
        @Query("to_date") to_date: String,
        @Query("eng_id") engineerId: String,
        @Query("pay_mode") pay_mode: String
    ): Single<CustomerAnalyticResponse>

}
