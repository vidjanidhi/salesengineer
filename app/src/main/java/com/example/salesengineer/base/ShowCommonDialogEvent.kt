package com.example.salesengineer.base

data class ShowCommonDialogEvent(
        val dialogMessage: String,
        val dialogTitle: String,
        val buttonAction: String
)
