package com.example.salesengineer.main.home

import android.location.Location
import com.example.customermanageadmin.realm_model.CustomerRepository
import com.example.salesengineer.R
import com.example.salesengineer.application.utils.NetworkState
import com.example.salesengineer.application.utils.NetworkStatus
import com.example.salesengineer.base.BaseServiceRxWrapper
import com.example.salesengineer.main.customers.CustomerAnalyticResponse
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.customers.livedata.CustomerListResponse
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import com.example.salesengineer.main.register.PaymentMode
import com.example.salesengineer.main.register.rx.AddEngineerResponse
import com.example.salesengineer.prefs.SharedPrefsService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*


class MainActivityPresenter(
    private val delegate: MainActivityViewDelegate,
    private val wrapper: RetrofitRxWrapper,
    private val customerRepository: CustomerRepository,
    private val isNetworkAvailable: Boolean,
    private val sharedPrefs: SharedPrefsService
) {
    private var disposable: Disposable? = null
    internal var locationServiceDialogShowing: Boolean = false
    internal var locationPermissionDialogShowing: Boolean = false
    val sdf = SimpleDateFormat("yyyy-MM-dd")

    val inputFormat = SimpleDateFormat("dd-MM-yyyy")
    val outputFormat = SimpleDateFormat("yyyy-MM-dd")
    val viewModel: MainActivityViewModel by lazy {
        this.delegate.getViewModel(MainActivityViewModel::class.java)
    }

    init {
        viewModel.filteData?.engineerId = delegate.getEngineerId()
    }

    fun applyFilter() {

        if (delegate.getFromDate() != "" && delegate.getTodate() != "") {
            val _to_date = inputFormat.parse("" + delegate.getTodate())
            val _from_date = inputFormat.parse("" + delegate.getFromDate())

            val to_date = sdf.parse("" + outputFormat.format(_to_date))  //to followup_date
            val frdate = sdf.parse("" + outputFormat.format(_from_date)) // from followup_date

            if (frdate.after(to_date)) {
                delegate.showMessage("Fromdate Should Be Less then todate")

            } else if (to_date.before(frdate)) {
                delegate.showMessage("Todate  Should Be More then Fromdate")

            } else {
                filterData()
            }
        } else {
            filterData()
        }


    }

    fun filterData() {
        viewModel.filteData?.name = delegate.getName()
        getDetail()
        delegate.dismissDialog()
    }

    fun handleInitialLoadingNetworkState(networkState: NetworkState) {
        when (networkState.status) {
            NetworkStatus.RUNNING -> this.delegate.showProgressBar()
            NetworkStatus.SUCCESS -> {
                this.delegate.hideProgressBar()
            }
            NetworkStatus.FAILED -> {
                this.delegate.hideProgressBar()
                if (networkState.message.isNotBlank()) {
                    this.delegate.showMessage(networkState.message)
                } else {
                    this.delegate.showMessage(R.string.error_email)
                }
            }
        }
    }

    fun getItems() {
        viewModel.filteData?.let {
            viewModel.initItems(
                it,
                wrapper,
                delegate.isConnectedToInternet()
            )
        }
        viewModel.customerListResponse?.let {
            viewModel.filteData?.let { it1 ->
                delegate.setAdapter(
                    it,
                    it1
                )
            }
        };

    }

    fun getDetail() {
        if (delegate.isConnectedToInternet()) {
            getAnalyticsDetails()
        } else {
            getAnalyticsDetailsFromRealm()
//            delegate.showMessage("You are offline")
        }
    }

    private fun getAnalyticsDetailsFromRealm() {

        var customerAnalyticResponse = CustomerAnalyticResponse()
        customerAnalyticResponse.total =
            viewModel?.filteData?.let { customerRepository.countTotalItemsFilter(it).toString() }
        customerAnalyticResponse.total_amount =
            viewModel?.filteData?.let { customerRepository.countTotalAmountFilter(it).toString() }
        viewModel.customerListResponse = customerAnalyticResponse
        success()
    }

    fun getAnalyticsDetails() {
        if (viewModel.customerListSingle == null) {
            viewModel.customerListSingle =
                viewModel.filteData?.let {
                    wrapper.getBrandItemListDetail(
                        it
                    )
                }
        }
        disposable?.dispose()

        disposable = this.viewModel.customerListSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<CustomerAnalyticResponse>() {
                override fun onSuccess(customerListResponse: CustomerAnalyticResponse) {

                    viewModel.customerListResponse = customerListResponse
                    viewModel.customerListSingle = null
                    if (customerListResponse.isSuccess == true) {
                        success()
                        delegate.dismissDialog()
                    } else {
                        success()
                        delegate.dismissDialog()
                        customerListResponse.message?.let { failed(it) }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.customerListSingle = null
                    var message: String
                    if (e is UnknownHostException) {
                        message = BaseServiceRxWrapper.NO_INTERNET_CONNECTION
                    } else {
                        message = e.message.toString()
                    }
                    failed(message)
                    dispose()
                }
            })
    }

    fun success() {
        getItems()
        delegate.hideProgressBar()
        viewModel.customerListResponse?.let {
            viewModel.filteData?.let { it1 ->
                delegate.setAdapter(
                    it,
                    it1
                )
            }
        };
    }

    fun failed(msg: String) {
        delegate.showMessage(msg)
    }

    fun saveToDate(newDate: Calendar) {
        viewModel.toDate = newDate
        viewModel.filteData?.to_Date = sdf.format(newDate.time)
        viewModel.filteData?.toDate = Date(newDate.getTimeInMillis())
    }

    fun saveFromDate(newDate: Calendar) {
        viewModel.fromDate = newDate
        viewModel.filteData?.from_date = sdf.format(newDate.time)
        viewModel.filteData?.fromDate = Date(newDate.getTimeInMillis())
    }

    fun storePaymentMode(i: String) {
        viewModel.paymentMode = PaymentMode.getPaymentMode(i)
        viewModel.filteData?.paymentMode = viewModel.paymentMode
    }

    fun setToday() {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        saveFromDate(calendar)


        calendar.set(Calendar.HOUR_OF_DAY,23)
        calendar.set(Calendar.MINUTE, 59)

        saveToDate(calendar)
    }

    fun setYesterDay() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, -1)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        saveFromDate(calendar)

        calendar.set(Calendar.HOUR_OF_DAY, 23)
        calendar.set(Calendar.MINUTE, 59)

        saveToDate(calendar)
    }

    fun setWeek() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, -7)
        saveFromDate(calendar)
        saveToDate(Calendar.getInstance())
    }

    fun customFilter() {
        removeName()
        removePaymentMode()
        removeFromDate()
        removeToDate()
    }

    fun removeFromDate() {
        viewModel.filteData?.to_Date = ""
        viewModel.filteData?.toDate = Date()
    }

    fun removeToDate() {
        viewModel.filteData?.from_date = ""
        viewModel.filteData?.fromDate = Date()
    }

    fun removeName() {
        viewModel.filteData?.name = ""
    }

    fun removePaymentMode() {
        viewModel.filteData?.paymentMode = 0
    }

    fun storeSelectedPosition(position: Int) {
        viewModel.selectedPosition = position
        delegate.notifyAdapter()
    }

    fun getSelectedPosition(): Int {
        return viewModel.selectedPosition
    }

    fun sync() {
        sendAddedCustomers()
    }

    fun getCustomerList() {
        if (viewModel.customerListSingle1 == null) {
            viewModel.customerListSingle1 =

                wrapper.getBrandItemListCall(sharedPrefs.engineerId)

        }
        disposable?.dispose()

        disposable = this.viewModel.customerListSingle1?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<CustomerListResponse>() {
                override fun onSuccess(customerListResponse: CustomerListResponse) {

                    viewModel.customerListResponse1 = customerListResponse
                    viewModel.customerListSingle1 = null
                    if (customerListResponse.isSuccess == true) {
                        storeToLocal()
                    } else {
                        sendAddedCustomers()
                        customerListResponse.message?.let { }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.customerListSingle1 = null
                    dispose()
                }
            })
    }

    fun storeToLocal() {
        customerRepository.deleteAllItems()
        var count = customerRepository.countTotalItems()

        viewModel.customerListResponse1?.payload?.let {
            customerRepository.insertOrUpdate(it)
        }
        var count1 = customerRepository.countTotalItems()
        delegate.hideProgressBar()
        ///over
    }

    fun sendAddedCustomers() {
        var engineers = customerRepository.findAdds()
        if (engineers.size > 0) {
            if (viewModel.lat == 0.0f && viewModel.lng == 0.0f) {
                delegate.getLocation()
            } else {
                addCustomerWrapper(engineers[0])
            }
        } else {
            sendEditedCustomers()
        }

    }

    fun addCustomerWrapper(customer: Customer) {

        if (viewModel.addResponseSingle == null) {
            viewModel.addResponseSingle =
                wrapper.addUser(
                    customer.getName(),
                    customer.getEmail(),
                    customer.getPhone(),
                    customer.amount.toString(),
                    customer.payment_mode.toInt(),
                    sharedPrefs.engineerId,
                    viewModel.lat.toString(),
                    viewModel.lng.toString()
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.addResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(loginResponse: AddEngineerResponse) {

                    viewModel.addResponse = loginResponse
                    viewModel.addResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        deleteAddedEngineer(customer.email)
                        sendAddedCustomers()
                    } else {
                        deleteAddedEngineer(customer.email)
                        sendAddedCustomers()
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.addResponseSingle = null
                    dispose()
                }
            })
    }

    private fun deleteAddedEngineer(email: String) {
        customerRepository.deleteAddedItem(email)
    }

    private fun sendEditedCustomers() {
        var engineers = customerRepository.findEdits()
        if (engineers.size > 0) {
            editCustomerWrapper(engineers[0])
        } else {
            success1()
        }
    }

    fun editCustomerWrapper(customer: Customer) {

        if (viewModel.editResponseSingle == null) {
            viewModel.editResponseSingle =
                wrapper.editUser(
                    customer?.itemId.toString(),
                    customer.getName(),
                    customer.getEmail(),
                    customer.getPhone(),
                    customer.amount.toString(),
                    customer.payment_mode.toInt(),
                    sharedPrefs.engineerId
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.editResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(editResponse: AddEngineerResponse) {

                    viewModel.editResponse = editResponse
                    viewModel.editResponseSingle = null
                    if (editResponse.isSuccess == true) {
                        deleteEditedEngineer(customer.itemId.toString())
                    } else {
                        deleteEditedEngineer(customer.itemId.toString())
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.editResponseSingle = null
                    dispose()
                }
            })
    }

    private fun deleteEditedEngineer(id: String) {
        customerRepository.deleteEditedItem(id)
        sendEditedCustomers()
    }

    private fun success1() {
        getCustomerList()

    }

    fun storeLocation(currentLocation: Location?) {
        viewModel.lat = currentLocation?.latitude?.toFloat()!!
        viewModel.lng = currentLocation.longitude.toFloat()
    }


}
