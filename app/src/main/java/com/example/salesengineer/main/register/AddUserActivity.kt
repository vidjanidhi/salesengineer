package com.example.salesengineer.main.register

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat
import com.example.salesengineer.R
import com.example.salesengineer.application.utils.PermissionRequestCodes
import com.example.salesengineer.application.utils.PermissionUtils
import com.example.salesengineer.main.StandardActivity
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import com.example.salesengineer.main.logout.LogoutActivit
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import deliverr.deliverrconsumer.utils.isValidEmail
import kotlinx.android.synthetic.main.activity_add_engineer.*


class AddUserActivity : StandardActivity(), AddUserDelegate {

    private var presenter: AddUserPresenter? = null
    var companies_list = PaymentMode.getPaymentModeList(false)
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.salesengineer.R.layout.activity_add_engineer)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle("Add User")

        bindViews()

        if (getIntent().hasExtra(KEY_USER)) {
            var engineer = getIntent().extras.getParcelable(KEY_USER) as Customer
            setView(engineer)
            supportActionBar?.setTitle("Edit User")
        }
    }

    private fun setView(customer: Customer) {
        addButton.setText("Edit")
        name.setText(customer?.name)
        email.setText(customer?.email)
        phone.setText(customer?.phone)
        amount.setText(customer?.amount.toString())

        if (customer.payment_mode.toInt() != 0) {
            for (i in companies_list.indices) {
                if (com.example.customermanageadmin.models.PaymentMode.getPaymentMode(companies_list[i]) == (customer.payment_mode.toInt())) {
                    payment_mode.setSelection(i)
                }
            }
        }

        presenter?.setEditMode(customer)
    }


    private fun bindViews() {
        presenter = customerRealmRepository?.let {
            AddUserPresenter(
                this,
                RetrofitRxWrapper(), it
            )
        }

        addButton.setOnClickListener({ view1 -> clickAddButton() })

        setSpinner()
    }

    private fun setSpinner() {

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, companies_list)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        payment_mode.setAdapter(adapter)



        payment_mode.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                presenter?.storePaymentMode(companies_list.get(i))
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {
            }
        })
    }

    override fun getEngineerId(): String? {
        return deliverrSharedPrefs?.engineerId
    }

    fun clickAddButton() {
        if (isUiValid()) {
            presenter?.addEngineer()
        }
    }

    fun clickBackButton() {
        finish()
    }

    private fun isUiValid(): Boolean {
        var isUiValid = true

        if (email.text.toString().trim { it <= ' ' }.isEmpty()) {
            emailInputLayout.requestFocus()
            emailInputLayout.isErrorEnabled = true
            emailInputLayout.error = getString(com.example.salesengineer.R.string.error_email)
            isUiValid = false
            return isUiValid
        } else {
            emailInputLayout.isErrorEnabled = false
        }

        if (!isValidEmail(email.text.toString())) {
            emailInputLayout.requestFocus()
            emailInputLayout.isErrorEnabled = true
            emailInputLayout.error = getString(com.example.salesengineer.R.string.error_valid_email)
            isUiValid = false
            return isUiValid
        } else {
            emailInputLayout.isErrorEnabled = false
        }

        if (name.text.toString().trim { it <= ' ' }.isEmpty()) {
            name.requestFocus()
            nameInputLayout.isErrorEnabled = true
            nameInputLayout.error = getString(com.example.salesengineer.R.string.error_name)
            isUiValid = false
            return isUiValid
        } else {
            nameInputLayout.isErrorEnabled = false
        }

        if (amount.text.toString().trim { it <= ' ' }.isEmpty()) {
            amount.requestFocus()
            amountInputLayout.isErrorEnabled = true
            amountInputLayout.error = getString(com.example.salesengineer.R.string.error_amount)
            isUiValid = false
            return isUiValid
        } else {
            amountInputLayout.isErrorEnabled = false
        }

        return isUiValid
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                clickBackButton()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun getEmail(): String {
        return email.text.toString()
    }

    override fun getName(): String {
        return name.text.toString()
    }

    override fun getPhone(): String {
        return phone.text.toString()
    }

    override fun getAmount(): String {
        return amount.text.toString()
    }

    override fun finishActivity() {
        finish()
    }

    override fun hideKeyboard() {
        deliverr.deliverrconsumer.utils.hideKeyboard(this)
    }

    override fun removeEmailError() {
        if (emailInputLayout.isErrorEnabled) {
            emailInputLayout.error = null
            emailInputLayout.isErrorEnabled = false
        }
    }

    override fun removeNameError() {
        if (nameInputLayout.isErrorEnabled) {
            nameInputLayout.error = null
            nameInputLayout.isErrorEnabled = false
        }
    }

    override fun removeAmountError() {
        if (amountInputLayout.isErrorEnabled) {
            amountInputLayout.error = null
            amountInputLayout.isErrorEnabled = false
        }
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun clearData() {
        email.setText("")
        amount.setText("")
        name.setText("")
        phone.setText("")
    }
    override fun getLocation() {
        if (this.handleLocationPermissions()) {
            mFusedLocationClient?.lastLocation?.addOnSuccessListener { location : Location? ->
                // Got last known location. In some rare situations this can be null.
                updateUI(location)
            }

        }
    }
    private fun updateUI( location : Location?) {
        presenter?.storeLocation(location)
        presenter?.addEngineerWrapper()
    }


    private fun handleLocationPermissions(): Boolean {
        if (!PermissionUtils.areLocationServicesEnabled(this)) {
            if (!presenter?.locationServiceDialogShowing!!) {       //condition to prevent overlap of location service dialog
                presenter?.locationServiceDialogShowing = true
                locationManager?.showEnableLocationDialog(this,
                    LogoutActivit.REQUEST_LOCATION_SERVICE
                )
            }
            return false
        } else {
            //Location Services are enabled
            //Permission Disabled
            if (!PermissionUtils.isLocationPermissionAndServiceGranted(this)) {
                if (PermissionUtils.checkIfUserHasDeniedLocationPermissionToNever(
                        deliverrSharedPrefs,
                        this
                    )
                ) {
                    //show SnackBar to open permissions settings
                    showMessage(R.string.enable_location_permission_deliveryAddress_snackbar_label)
                    return false
                }
                if (PermissionUtils.shouldShowLocationPermissionRationale(this)) {
                    //show SnackBar to again popup permissions dialog
                    showMessage(R.string.enable_location_permission_deliveryAddress_snackbar_label)
                } else {
                    //asking for permission first time
                    presenter?.locationPermissionDialogShowing = true
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        PermissionRequestCodes.REQUEST_LOCATION
                    )
                }

                return false
            } else {
                //Location services enabled
                //Permission enabled
                return true
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LogoutActivit.REQUEST_LOCATION_SERVICE) {
            this.presenter?.locationServiceDialogShowing = false

            if (resultCode == Activity.RESULT_OK) {
                //location service enabled > check for permission
                this.getLocation()
            } else {
                //location service disabled > show snackBar
                if (!PermissionUtils.areLocationServicesEnabled(this)) {
                    showMessage(R.string.msg_request_location_service)
                }
            }
        }
    }

    companion object {
        private const val KEY_USER = "key-user"
        fun getLauncherIntent(context: Context): Intent {
            return Intent(context, AddUserActivity::class.java)
        }


        fun launch(context: Context, engineer: Customer) {
            context.startActivity(getLauncherIntent(context, engineer))
        }

        fun launch(context: Context) {
            context.startActivity(getLauncherIntent(context))
        }

        fun getLauncherIntent(
            context: Context,
            engineer: Customer
        ): Intent {
            return Intent(context, AddUserActivity::class.java).apply {
                putExtra(KEY_USER, engineer)
            }
        }
    }
}
