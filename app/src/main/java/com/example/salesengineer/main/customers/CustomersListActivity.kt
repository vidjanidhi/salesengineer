package com.example.salesengineer.main.customers

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.customermanageadmin.realm_model.CustomerRealmRepository
import com.example.salesengineer.R
import com.example.salesengineer.application.utils.RetryListener
import com.example.salesengineer.main.StandardActivity
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.customers.livedata.CustomerPagedListAdapter
import com.example.salesengineer.main.customers.livedata.CustomerViewHolder
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import com.example.salesengineer.main.register.AddUserActivity
import deliverr.deliverrconsumer.referral.account_status.CustomerListPresenter
import deliverr.deliverrconsumer.referral.account_status.CustomerListViewDelegate
import kotlinx.android.synthetic.main.activity_customers_list.*

class CustomersListActivity : StandardActivity(), CustomerListViewDelegate, RetryListener,
    CustomerViewHolder.CustomerClickListner {
    override fun onClickEngineer(customer: Customer) {
        AddUserActivity.launch(this, customer)
    }

    override fun retry() {
        this.getViewModel(CustomerListViewModel::class.java).retry()
    }

    private lateinit var presenter: CustomerListPresenter
    private lateinit var layoutManager: LinearLayoutManager
    private var menuItemsAdapter: CustomerPagedListAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customers_list)

        this.setSupportActionBar(toolbar)

        if (this.supportActionBar != null) {
            this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        customerRealmRepository = realm?.let { CustomerRealmRepository(it) }
        this.presenter =
            customerRealmRepository?.let {
                CustomerListPresenter(
                    this,
                    RetrofitRxWrapper(),
                    it,
                    isConnectedToInternet()
                )
            }!!

    }

    override fun setAdapter(customerListResponse: CustomerAnalyticResponse) {
        setListAdapter(customerListResponse)
    }

    override fun onResume() {
        super.onResume()
        presenter.getDetail()
    }
    override fun getEngineerId(): String {
       return deliverrSharedPrefs?.engineerId.toString()
    }

    override fun showProgressBar() {
        recyclerView_transactions.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        recyclerView_transactions.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }


    private fun setListAdapter(customerListResponse: CustomerAnalyticResponse) {
        val viewModel: CustomerListViewModel = this.getViewModel(CustomerListViewModel::class.java)

        this.menuItemsAdapter =
            CustomerPagedListAdapter(customerListResponse,this)

        viewModel.initialLoading.observe(this, Observer {
            this.presenter.handleInitialLoadingNetworkState(it)
        })

        viewModel.networkState.observe(this, Observer {
            this.menuItemsAdapter?.setNetworkState(it)
        })

        viewModel.menuItemsLiveData.observe(this, Observer {
            this.menuItemsAdapter?.submitList(it)
        })

        this.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL,
            false
        )

        recyclerView_transactions.layoutManager = this.layoutManager
        recyclerView_transactions.adapter = this.menuItemsAdapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        fun getLauncherIntent(context: Context): Intent {
            return Intent(context, CustomersListActivity::class.java)
        }

        @JvmStatic
        fun launch(context: Context) {
            context.startActivity(getLauncherIntent(context))
        }
    }


}
