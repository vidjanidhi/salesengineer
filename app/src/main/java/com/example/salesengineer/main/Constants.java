package com.example.salesengineer.main;

import com.example.salesengineer.prefs.SharedPrefsService;

import javax.inject.Inject;


public class Constants {
    @Inject
    SharedPrefsService deliverrSharedPrefs;

    @Inject
    public Constants() {
    }

    public static final String base_url = "https://appnutzz.com/server/customer_sales/";

    public Constants(SharedPrefsService deliverrSharedPrefs) {
        this.deliverrSharedPrefs = deliverrSharedPrefs;
    }

    public static class Admin {
        public static final String LOGIN = "api.php?req=engineer_login";
        public static final String ADD_ENGINEER = "api.php?req=engineer_insert";
        public static final String ADD_USER = "api.php?req=user_insert";
        public static final String EDIT_USER = "api.php?req=user_update";
        public static final String GET_CUSTOMERS = "api.php?req=get_customers";
        public static final String GET_CUSTOMERS_DETAIL = "api.php?req=get_customers_detail";

    }


}
