package com.example.salesengineer.main.login.rx

import com.example.salesengineer.base.BaseServiceRxWrapper
import com.example.salesengineer.base.RetrofitService
import com.example.salesengineer.main.customers.CustomerAnalyticResponse
import com.example.salesengineer.main.customers.livedata.CustomerListResponse
import com.example.salesengineer.main.home.livedata.FilterData
import com.example.salesengineer.main.register.rx.AddEngineerResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Call

class RetrofitRxWrapper : BaseServiceRxWrapper() {
    private val service: RetrofitService = this.retrofit.create(RetrofitService::class.java)

    fun adminLogin(
        email: String,
        password: String

    ): Single<AdminLoginResponse> {
        return this.service.adminLogin(email, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .cache()
    }

    fun addUser(
        name: String,
        email: String,
        phone: String,
        amount: String,
        payment_mode: Int,
        engineerId: String,
        lat: String,
        lng: String

    ): Single<AddEngineerResponse> {
        return this.service.addUser(name, email, phone, amount, engineerId, payment_mode,lat,lng)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .cache()
    }

    fun editUser(
        id:String,
        name: String,
        email: String,
        phone: String,
        amount: String,
        payment_mode: Int,
        engineerId: String

    ): Single<AddEngineerResponse> {
        return this.service.editUser(id,name, email, phone, amount, engineerId, payment_mode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .cache()
    }


    fun getBrandItemListCall(
        engineerId: String,
        skip: Int,
        take: Int
    ): Call<CustomerListResponse> {
        return this.service.getBrandItemList(
            engineerId,
            skip,
            take
        )
    }

    fun getBrandItemListCall(
        filterData: FilterData,
        skip: Int,
        take: Int
    ): Call<CustomerListResponse> {
        return this.service.getBrandItemList(
            filterData.name,
            "custom",
            filterData.from_date,
            filterData.to_Date,
            filterData.engineerId,
            getPay_mode(filterData.paymentMode),
            skip,
            take
        )
    }
    fun getBrandItemListCall(
        engineerId: String

    ): Single<CustomerListResponse> {
        return this.service.getBrandItemList(
engineerId
        )
    }


    fun getBrandItemListDetail(
        filterData: FilterData
    ): Single<CustomerAnalyticResponse> {
        return this.service.getBrandItemListDetail(
            filterData.name,
            "custom",
            filterData.from_date,
            filterData.to_Date,
            filterData.engineerId,
            getPay_mode(filterData.paymentMode)
        )
    }

    fun getBrandItemListDetail(engineerId: String): Single<CustomerAnalyticResponse> {
        return this.service.getBrandItemListDetail(engineerId)
    }

    fun getPay_mode(payMode: Int): String {
        return if (payMode != 0) {
            payMode.toString()
        } else {
            ""
        }
    }

}
