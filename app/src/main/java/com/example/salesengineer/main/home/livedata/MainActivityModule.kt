package com.example.salesengineer.main.home.livedata

import androidx.lifecycle.LifecycleOwner
import com.example.salesengineer.main.home.MainActivity
import com.example.salesengineer.main.home.MainActivityViewDelegate
import dagger.Module
import dagger.Provides
import deliverr.deliverrconsumer.di.scopes.ActivityScope

@Module
class MainActivityModule {
    @Provides
    @ActivityScope
    fun provideMainActivityViewDelegate(activity: MainActivity): MainActivityViewDelegate =
        activity

    @Provides
    @ActivityScope
    fun provideLifecycleOwner(activity: MainActivity): LifecycleOwner = activity
}
