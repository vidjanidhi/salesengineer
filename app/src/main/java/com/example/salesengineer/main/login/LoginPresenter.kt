package com.example.salesengineer.main.login

import android.text.Editable
import android.text.TextWatcher

import com.example.salesengineer.R
import com.example.salesengineer.base.BaseServiceRxWrapper
import com.example.salesengineer.main.login.rx.AdminLoginResponse
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.net.UnknownHostException

internal class LoginPresenter(
    private val loginDelegate: LoginDelegate,
    private val wrapper: RetrofitRxWrapper
) : TextWatcher {

    val viewModel: LoginViewModel by lazy {
        this.loginDelegate.getViewModel(LoginViewModel::class.java)
    }
    private var disposable: Disposable? = null

    fun login() {
        if (loginDelegate.isConnectedToInternet()) {
            loginDelegate.hideKeyboard()
            loginDelegate.showProgressBar()
            adminLogin()
        } else {
            loginDelegate.showMessage(R.string.no_internet_connection_message)
        }
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        val text = s.toString()
        if (text.length > 0) {
            if (text == loginDelegate.getEmail()) {
                loginDelegate.removeEmailError()
            }
            if (text == loginDelegate.getPassword()) {
                loginDelegate.removePasswordError()
            }
        }
    }

    override fun afterTextChanged(s: Editable) {

    }

    fun adminLogin() {

            if (viewModel.loginSingle == null) {
                viewModel.loginSingle =
                    wrapper.adminLogin(loginDelegate.getEmail(), loginDelegate.getPassword())
            }
            disposable?.dispose()

            disposable = this.viewModel.loginSingle?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeWith(object : DisposableSingleObserver<AdminLoginResponse>() {
                    override fun onSuccess(loginResponse: AdminLoginResponse) {

                        viewModel.loginResponse = loginResponse
                        viewModel.loginSingle = null
                        if (loginResponse.isSuccess == true) {
                            loginSuccess()
                        } else {
                            loginResponse.message?.let { loginFailed(it) }
                        }
                    }

                    override fun onError(e: Throwable) {
                        viewModel.loginSingle = null
                        var message: String
                        if (e is UnknownHostException) {
                            message = BaseServiceRxWrapper.NO_INTERNET_CONNECTION
                        } else {
                            message = e.message.toString()
                        }
                        loginFailed(message)
                        dispose()
                    }
                })
    }

    private fun loginSuccess() {
        loginDelegate.hideProgressBar()
        viewModel.loginResponse?.payload?.email?.let {
            viewModel.loginResponse?.payload?.id?.let { it1 ->
                loginDelegate.saveAdminDetail(
                    it1,
                    it
                )
            }
        }
        loginDelegate.redirectUser()
    }

    private fun loginFailed(message: String) {
        loginDelegate.hideProgressBar()
        loginDelegate.showMessage(message)
    }

}
