package com.example.salesengineer.main.customers.livedata;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.recyclerview.widget.DiffUtil;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

@RealmClass
public class Customer implements Parcelable, RealmModel {
    @SerializedName("id")
    private String itemId;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("amount")
    private Float amount;
    @SerializedName("payment_mode")
    private String payment_mode;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;
    @SerializedName("created")
    private Date created;

    private  Boolean isEdit=false;
    private  Boolean isAdd=false;

    public Boolean getEdit() {
        return isEdit;
    }

    public void setEdit(Boolean edit) {
        isEdit = edit;
    }

    public Boolean getAdd() {
        return isAdd;
    }

    public void setAdd(Boolean add) {
        isAdd = add;
    }

    protected Customer(Parcel in) {
        itemId = in.readString();
        name = in.readString();
        email = in.readString();
        phone = in.readString();
        if (in.readByte() == 0) {
            amount = null;
        } else {
            amount = in.readFloat();
        }
        payment_mode = in.readString();
        lat = in.readString();
        lng = in.readString();
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Customer() {
    }

    public long getItemId() {
        return Long.parseLong(itemId);
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

  /*  public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }*/

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(itemId);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(phone);
        if (amount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeFloat(amount);
        }
        dest.writeString(payment_mode);
        dest.writeString(lat);
        dest.writeString(lng);
    }



    public static DiffUtil.ItemCallback<Customer> getDiffCallback() {
        return DIFF_CALLBACK;
    }
    public static final DiffUtil.ItemCallback<Customer> DIFF_CALLBACK = new DiffUtil.ItemCallback<Customer>() {
        @Override
        public boolean areItemsTheSame(Customer oldItem, Customer newItem) {
            return (oldItem.getItemId() == newItem.getItemId());
        }

        @Override
        public boolean areContentsTheSame(Customer oldItem, Customer newItem) {
            return (oldItem.getItemId() == newItem.getItemId());
        }
    };


    public static Creator<Customer> getCREATOR() {
        return CREATOR;
    }
}
