package com.example.salesengineer.main.login.rx

import com.google.gson.annotations.SerializedName

class AdminLoginResponse {
    @SerializedName("success")
    val isSuccess: Boolean = false
    @SerializedName("message")
    val message: String? = null
    @SerializedName("payload")
    val payload: Payload? = null
}
