package com.example.salesengineer.main.customers.livedata

import android.text.util.Linkify
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.salesengineer.R
import com.example.salesengineer.main.register.PaymentMode
import deliverr.deliverrconsumer.utils.roundOffDecimal
import java.math.RoundingMode
import java.text.DecimalFormat

class CustomerViewHolder(
    rootView: View,
    private val clickListener: CustomerClickListner
) : RecyclerView.ViewHolder(rootView) {
    private val name: AppCompatTextView = rootView.findViewById(R.id.title)
    private val email: AppCompatTextView = rootView.findViewById(R.id.email)
    private val phone: AppCompatTextView = rootView.findViewById(R.id.phone)
    private val payment_mode: AppCompatTextView = rootView.findViewById(R.id.payment_mode)
    private val amount: AppCompatTextView = rootView.findViewById(R.id.amount)

    fun bind(item: Customer) {
        name.setText(item.name)
        email.setText("Email : " + item.email)
        Linkify.addLinks(email, Linkify.EMAIL_ADDRESSES)
        phone.setText("Phone : " + item.phone)
        Linkify.addLinks(phone, Linkify.PHONE_NUMBERS)
        payment_mode.setText("Payment Mode : " + PaymentMode.getStringPaymentMode(item.payment_mode.toInt()))
        amount.setText(
            "₹ " + roundOffDecimal(item.amount)
        )

        itemView.findViewById<AppCompatImageView>(R.id.edit_button).setOnClickListener {
            clickListener.onClickEngineer(item)
        }
    }
    interface CustomerClickListner {
        fun onClickEngineer(customer: Customer)
    }



}
