package com.example.salesengineer.main.customers;

import com.google.gson.annotations.SerializedName;

public class CustomerAnalyticResponse {
    @SerializedName("success")
     boolean success;
    @SerializedName("message")
     String message;
    @SerializedName("total")
     String total;
    @SerializedName("total_amount")
    String total_amount;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }


}
