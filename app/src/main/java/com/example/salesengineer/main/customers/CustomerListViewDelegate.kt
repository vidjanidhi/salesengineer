package deliverr.deliverrconsumer.referral.account_status

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import com.example.salesengineer.main.customers.CustomerAnalyticResponse

interface CustomerListViewDelegate {

    fun showProgressBar()
    fun hideProgressBar()
    fun getEngineerId():String

    fun showMessage(message: String)
    fun showMessage(@StringRes messageId: Int)

    fun isConnectedToInternet(): Boolean
    fun <T : ViewModel> getViewModel(clazz: Class<T>): T

    fun setAdapter(customerListResponse: CustomerAnalyticResponse)
}
