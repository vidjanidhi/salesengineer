package deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.home.livedata.FilterData
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper

class MainDataSourceFactory(
   private val filterData: FilterData,
    private val wrapper: RetrofitRxWrapper,
   private val isNetworkAvailable: Boolean
) : DataSource.Factory<Int, Customer>() {
    val mainDataSourceLiveData: MutableLiveData<MainDataSource>
    private lateinit var brandItemsDataSource: MainDataSource

    init {
        this.mainDataSourceLiveData = MutableLiveData()
    }

    override fun create(): DataSource<Int, Customer> {
        this.brandItemsDataSource = MainDataSource(
            filterData,
            this.wrapper,isNetworkAvailable
        )
        this.mainDataSourceLiveData.postValue(this.brandItemsDataSource)
        return this.brandItemsDataSource
    }
}
