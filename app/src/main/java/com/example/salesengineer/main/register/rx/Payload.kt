package com.example.salesengineer.main.register.rx

import com.google.gson.annotations.SerializedName


class Payload {
    @SerializedName("id")
    val id: String? = null

    @SerializedName("email")
    val email: String? = null

    @SerializedName("name")
    val name: String? = null

    @SerializedName("address")
    val address: String? = null

    @SerializedName("phone")
    val phone: String? = null

}
