package com.example.salesengineer.main.logout

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.AsyncTask
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import com.example.customermanageadmin.main.logout.LogoutDelegate
import com.example.customermanageadmin.main.logout.LogoutPresenter
import com.example.salesengineer.R
import com.example.salesengineer.application.utils.PermissionRequestCodes
import com.example.salesengineer.application.utils.PermissionUtils
import com.example.salesengineer.di.location.LocationManager
import com.example.salesengineer.main.StandardActivity
import com.example.salesengineer.main.login.LoginActivity
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import com.google.android.gms.location.*

class LogoutActivit : StandardActivity(), LogoutDelegate {
    private var presenter: LogoutPresenter? = null


    private var mFusedLocationClient: FusedLocationProviderClient? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logout)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)


        if (presenter == null) {
            this.presenter = customerRealmRepository?.let {

                deliverrSharedPrefs?.let { it2 ->
                    LogoutPresenter(
                        this,
                        it,
                        RetrofitRxWrapper(),
                        it2

                    )

                }
            }
        }
        presenter?.initialize()


    }




    override fun finishCurrent() {
        finish()
    }

    override fun showSignInScreen() {
        LoginActivity.startControllerNewTask(
            this
        )
    }

    override fun showErrorMessage(message: String) {
        showMessage(message)
    }

    override fun clearPreferences() {
        deliverrSharedPrefs?.clearPreferences()
    }

    override fun getLocation() {
        if (this.handleLocationPermissions()) {
            mFusedLocationClient?.lastLocation?.addOnSuccessListener { location : Location? ->
                // Got last known location. In some rare situations this can be null.
                updateUI(location)
            }

        }
    }

    private fun updateUI( location : Location?) {
        presenter?.storeLocation(location)
        presenter?.sendAddedCustomers()
    }


    private fun handleLocationPermissions(): Boolean {
        if (!PermissionUtils.areLocationServicesEnabled(this)) {
            if (!presenter?.locationServiceDialogShowing!!) {       //condition to prevent overlap of location service dialog
                presenter?.locationServiceDialogShowing = true
                locationManager?.showEnableLocationDialog(this, REQUEST_LOCATION_SERVICE)
            }
            return false
        } else {
            //Location Services are enabled
            //Permission Disabled
            if (!PermissionUtils.isLocationPermissionAndServiceGranted(this)) {
                if (PermissionUtils.checkIfUserHasDeniedLocationPermissionToNever(
                        deliverrSharedPrefs,
                        this
                    )
                ) {
                    //show SnackBar to open permissions settings
                    showMessage(R.string.enable_location_permission_deliveryAddress_snackbar_label)
                    return false
                }
                if (PermissionUtils.shouldShowLocationPermissionRationale(this)) {
                    //show SnackBar to again popup permissions dialog
                    showMessage(R.string.enable_location_permission_deliveryAddress_snackbar_label)
                } else {
                    //asking for permission first time
                    presenter?.locationPermissionDialogShowing = true
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        PermissionRequestCodes.REQUEST_LOCATION
                    )
                }

                return false
            } else {
                //Location services enabled
                //Permission enabled
                return true
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_LOCATION_SERVICE) {
            this.presenter?.locationServiceDialogShowing = false

            if (resultCode == Activity.RESULT_OK) {
                //location service enabled > check for permission
                this.getLocation()
            } else {
                //location service disabled > show snackBar
                if (!PermissionUtils.areLocationServicesEnabled(this)) {
                    showMessage(R.string.msg_request_location_service)
                }
            }
        }
    }



    companion object {
        const val REQUEST_LOCATION_SERVICE = 8081
        fun launch(context: Context) {
            val launcherIntent = Intent(context, LogoutActivit::class.java)
            context.startActivity(launcherIntent)
        }
    }

}