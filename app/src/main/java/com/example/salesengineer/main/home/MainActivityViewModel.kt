package com.example.salesengineer.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.salesengineer.application.utils.NetworkState
import com.example.salesengineer.main.customers.CustomerAnalyticResponse
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.customers.livedata.CustomerListResponse
import com.example.salesengineer.main.home.livedata.FilterData
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import com.example.salesengineer.main.register.rx.AddEngineerResponse
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.FUTURE_PAGE_SIZE
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.INITIAL_PAGE_SIZE
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.MainDataSource
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.MainDataSourceFactory
import io.reactivex.Single
import java.util.*

class MainActivityViewModel : ViewModel() {

    var selectedPosition: Int=0
    var filteData= FilterData()
    var paymentMode: Int = 0


    var customerListSingle1: Single<CustomerListResponse>? = null
    var customerListResponse1: CustomerListResponse?=null

    var lat: Float=0.0f;
    var lng: Float=0.0f;

    var editResponseSingle: Single<AddEngineerResponse>? = null
    var editResponse: AddEngineerResponse?=null

    var addResponseSingle: Single<AddEngineerResponse>? = null
    var addResponse: AddEngineerResponse?=null

    var fromDate: Calendar = Calendar.getInstance()
    var toDate: Calendar = Calendar.getInstance()
    var customerListSingle: Single<CustomerAnalyticResponse>? = null
    var customerListResponse: CustomerAnalyticResponse? = null

    lateinit var initialLoading: LiveData<NetworkState>
    lateinit var networkState: LiveData<NetworkState>
    lateinit var menuItemsLiveData: LiveData<PagedList<Customer>>

    lateinit var mainDataSource: LiveData<MainDataSource>

    fun retry() {
        this.mainDataSource.value!!.retryLoading()
    }

    fun initItems(
        filterData: FilterData,
        wrapper: RetrofitRxWrapper,
        isNetworkAvailable: Boolean
    ) {
        val brandItemsDataSourceFactory = MainDataSourceFactory(
            filterData,
            wrapper,isNetworkAvailable
        )
        this.mainDataSource = brandItemsDataSourceFactory.mainDataSourceLiveData

        this.initialLoading = Transformations.switchMap(
            brandItemsDataSourceFactory.mainDataSourceLiveData
        ) { it.initialLoading }
        this.networkState = Transformations.switchMap(
            brandItemsDataSourceFactory.mainDataSourceLiveData
        ) { it.networkState }

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(INITIAL_PAGE_SIZE)
            .setPageSize(FUTURE_PAGE_SIZE)
            .setPrefetchDistance(FUTURE_PAGE_SIZE)
            .build()

        this.menuItemsLiveData = LivePagedListBuilder(brandItemsDataSourceFactory, pagedListConfig)
            .build()
    }

}
