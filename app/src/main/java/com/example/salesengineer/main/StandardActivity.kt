package com.example.salesengineer.main

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.example.customermanageadmin.realm_model.CustomerRealmRepository
import com.example.customermanageadmin.realm_model.EngineerRealmRepository
import com.example.customermanageadmin.receiver.ConnectivityReceiver
import com.example.salesengineer.di.location.LocationManager
import com.example.salesengineer.prefs.SharedPrefsService
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import io.realm.Realm
import javax.inject.Inject


abstract class StandardActivity : DaggerAppCompatActivity(),

    ConnectivityReceiver.ConnectivityReceiverListener
{
    private var mSnackBar: Snackbar? = null
    var isConnected: Boolean = false


    @set:Inject
    var deliverrSharedPrefs: SharedPrefsService? = null
    @set:Inject
    var realm: Realm? = null
    @set:Inject
    var constants: Constants? = null
    var locationManager: LocationManager? = null

    @set:Inject
    var customerRealmRepository: CustomerRealmRepository? = null

  /*  @set:Inject
    var engineerRealmRepository: EngineerRealmRepository? = null
*/
    var receiver: ConnectivityReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationManager = LocationManager(this)

        receiver = ConnectivityReceiver()
        registerReceiver(
            receiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }


    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }


    /*private void logUserOut(String message) {
        Intent intent = new Intent(this, LogoutActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        this.startActivity(intent);
        this.showMessage(message);
    }*/

    fun showMessage(@StringRes messageId: Int) {
        this.showMessage(this.getString(messageId))
    }

    fun showMessage(message: String) {
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }
    }

    fun <T : ViewModel> getViewModel(clazz: Class<T>): T {
        return ViewModelProviders.of(this).get(clazz)
    }

    open fun isConnectedToInternet(): Boolean {
//        return isConnected
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        if (activeNetwork == null) {
            // connected to the internet
            return false
        }
        // not connected to the internet
        return true
    }

    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {
            Toast.makeText(this, " You are offline", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, " You are Online now", Toast.LENGTH_LONG).show()
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        this.isConnected = isConnected
//        showMessage(isConnected)
    }


}
