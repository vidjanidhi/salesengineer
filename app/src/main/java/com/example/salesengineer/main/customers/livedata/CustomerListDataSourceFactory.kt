package deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper

class CustomerListDataSourceFactory(
    private val wrapper: RetrofitRxWrapper,
    private val engineerId: String,
    private val isNetworkAvailable: Boolean
) : DataSource.Factory<Int, Customer>() {
    val brandItemsDataSourceLiveData: MutableLiveData<CustomerListDataSource>
    private lateinit var brandItemsDataSource: CustomerListDataSource

    init {
        this.brandItemsDataSourceLiveData = MutableLiveData()
    }

    override fun create(): DataSource<Int, Customer> {
        this.brandItemsDataSource = CustomerListDataSource(
            this.wrapper,
            engineerId,isNetworkAvailable
        )
        this.brandItemsDataSourceLiveData.postValue(this.brandItemsDataSource)
        return this.brandItemsDataSource
    }
}
