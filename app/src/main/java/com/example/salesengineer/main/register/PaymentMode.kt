package com.example.salesengineer.main.register

class PaymentMode(
) {

    companion object {
        const val STRIPE = "STRIPE"
        const val PAYTM = "PAYTM"
        const val PAYPAL = "PAYPAL"
        const val OTHER = "OTHER"

        fun getPaymentMode(mode: String): Int {
            if (mode.equals(STRIPE)) {
                return 1;
            } else if (mode.equals(PAYTM)) {
                return 2;
            } else if (mode.equals(PAYPAL)) {
                return 3;
            } else if (mode.equals(OTHER)) {
                return 4;
            } else {
                return 0;
            }
        }

        fun getPaymentModeList(isHeaderAdded: Boolean): ArrayList<String> {
            var companies_list = ArrayList<String>()

            if (isHeaderAdded) {
                companies_list.add("ADD PAYMENT MODE")
            }

            companies_list.add(STRIPE)
            companies_list.add(PAYTM)
            companies_list.add(PAYPAL)
            companies_list.add(OTHER)

            return companies_list
        }

        fun getStringPaymentMode(mode: Int): String {
            return when (mode) {
                1 -> STRIPE
                2 -> PAYTM
                3 -> PAYPAL
                4 -> OTHER
                else -> OTHER

            }
        }
    }

}
