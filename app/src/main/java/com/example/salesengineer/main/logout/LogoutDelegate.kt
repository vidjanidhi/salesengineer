package com.example.customermanageadmin.main.logout

import androidx.lifecycle.ViewModel

interface LogoutDelegate {

    fun showSignInScreen()

    fun showErrorMessage(message: String)
    fun isConnectedToInternet(): Boolean
    fun <T : ViewModel> getViewModel(clazz: Class<T>): T
    fun clearPreferences()
    fun finishCurrent()
//    fun startFetchAccurateLocationTask()
    fun getLocation()
}
