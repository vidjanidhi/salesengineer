package com.example.salesengineer.main.splash

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.os.Handler
import androidx.core.app.ActivityCompat
import com.example.customermanageadmin.main.splash.SplashActivityPresenter
import com.example.customermanageadmin.main.splash.SplashDelegate
import com.example.salesengineer.R
import com.example.salesengineer.application.utils.PermissionRequestCodes
import com.example.salesengineer.application.utils.PermissionUtils
import com.example.salesengineer.main.StandardActivity
import com.example.salesengineer.main.home.MainActivity
import com.example.salesengineer.main.login.LoginActivity
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import com.example.salesengineer.main.logout.LogoutActivit
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

class SplashActivity : StandardActivity(), SplashDelegate {
    private val SPLASH_DISPLAY_LENGTH = 2500
    private var presenter: SplashActivityPresenter? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        this.presenter = customerRealmRepository?.let {

            deliverrSharedPrefs?.let { it2 ->
                SplashActivityPresenter(
                    this,
                    RetrofitRxWrapper(),
                    it,
                    it2
                )
            }
        }
    }

    override fun openNext() {
        this.startNextActivity()
    }

    private fun startHomeActivity() {
        MainActivity.launch(this)
        finish()
    }

    private fun startLoginActivity() {
        LoginActivity.launch(this)
        finish()
    }

    override fun onPause() {
        super.onPause()
        cleanUp()
    }

    internal fun cleanUp() {
        if (realm != null && !realm!!.isClosed()) {
            realm!!.close()
        }
    }

    override fun onResume() {
        super.onResume()

        Handler().postDelayed(
            {
                if (isConnectedToInternet()) {
                    presenter?.initialize()
                } else {
                    var count = customerRealmRepository?.countTotalItems()
                    if (count == 0) {
                        showMessage("Connect Internet")
                        finish()
                    } else {
                        openNext()
                    }
                }
            },
            SPLASH_DISPLAY_LENGTH.toLong()
        )  //delay to show activity_splash screen
    }

    fun startNextActivity() {
        if (deliverrSharedPrefs?.isLogin!!) {
            startHomeActivity()
        } else {
            startLoginActivity()
        }

    }
    override fun getLocation() {
        if (this.handleLocationPermissions()) {
            mFusedLocationClient?.lastLocation?.addOnSuccessListener { location : Location? ->
                // Got last known location. In some rare situations this can be null.
                updateUI(location)
            }

        }
    }

    private fun updateUI( location : Location?) {
        presenter?.storeLocation(location)
        presenter?.sendAddedCustomers()
    }


    private fun handleLocationPermissions(): Boolean {
        if (!PermissionUtils.areLocationServicesEnabled(this)) {
            if (!presenter?.locationServiceDialogShowing!!) {       //condition to prevent overlap of location service dialog
                presenter?.locationServiceDialogShowing = true
                locationManager?.showEnableLocationDialog(this,
                    LogoutActivit.REQUEST_LOCATION_SERVICE
                )
            }
            return false
        } else {
            //Location Services are enabled
            //Permission Disabled
            if (!PermissionUtils.isLocationPermissionAndServiceGranted(this)) {
                if (PermissionUtils.checkIfUserHasDeniedLocationPermissionToNever(
                        deliverrSharedPrefs,
                        this
                    )
                ) {
                    //show SnackBar to open permissions settings
                    showMessage(R.string.enable_location_permission_deliveryAddress_snackbar_label)
                    return false
                }
                if (PermissionUtils.shouldShowLocationPermissionRationale(this)) {
                    //show SnackBar to again popup permissions dialog
                    showMessage(R.string.enable_location_permission_deliveryAddress_snackbar_label)
                } else {
                    //asking for permission first time
                    presenter?.locationPermissionDialogShowing = true
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        PermissionRequestCodes.REQUEST_LOCATION
                    )
                }

                return false
            } else {
                //Location services enabled
                //Permission enabled
                return true
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LogoutActivit.REQUEST_LOCATION_SERVICE) {
            this.presenter?.locationServiceDialogShowing = false

            if (resultCode == Activity.RESULT_OK) {
                //location service enabled > check for permission
                this.getLocation()
            } else {
                //location service disabled > show snackBar
                if (!PermissionUtils.areLocationServicesEnabled(this)) {
                    showMessage(R.string.msg_request_location_service)
                }
            }
        }
    }


}
