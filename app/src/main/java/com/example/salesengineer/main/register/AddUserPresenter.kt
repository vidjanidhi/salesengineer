package com.example.salesengineer.main.register

import android.location.Location
import android.text.Editable
import android.text.TextWatcher
import com.example.customermanageadmin.realm_model.CustomerRepository
import com.example.salesengineer.base.BaseServiceRxWrapper
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import com.example.salesengineer.main.register.rx.AddEngineerResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.net.UnknownHostException
import java.util.*

internal class AddUserPresenter(
    private val delegate: AddUserDelegate,
    private val wrapper: RetrofitRxWrapper,
    private val customerRepository: CustomerRepository
) : TextWatcher {
    internal var locationServiceDialogShowing: Boolean = false
    internal var locationPermissionDialogShowing: Boolean = false
    val viewModel: AddUserViewModel by lazy {
        this.delegate.getViewModel(AddUserViewModel::class.java)
    }
    private var disposable: Disposable? = null

    fun addEngineer() {
        if (viewModel.isEditMode) {
            if (delegate.isConnectedToInternet()) {
                delegate.hideKeyboard()
                delegate.showProgressBar()
                editEngineerWrapper()
            } else {
                editEngineerWrapperRealm()
//                delegate.showMessage(R.string.no_internet_connection_message)
            }
        } else {
            if (delegate.isConnectedToInternet()) {
                delegate.hideKeyboard()
                delegate.showProgressBar()
                if (viewModel.lat == 0.0f && viewModel.lng == 0.0f) {
                    delegate.getLocation()
                } else {
                    addEngineerWrapper()
                }

            } else {
                addEngineerWrapperRealm()
//                delegate.showMessage(R.string.no_internet_connection_message)
            }
        }
    }

    fun editEngineerWrapperRealm() {
        var customer = Customer()

        customer.setItemId(viewModel.customer?.itemId!!.toString())
        customer.name = delegate.getName()
        customer.email = delegate.getEmail()
        customer.phone = delegate.getPhone()
        customer.amount = delegate.getAmount().toFloat()
        customer.payment_mode = viewModel.payment_mode.toString()
        customer.created = Date()

//        engineerRepository.insertOrUpdate(engineer)

        customer.edit = true

        var count1 = customerRepository.countTotalItems()
        customerRepository.insertEditedItem(customer, customer.itemId.toString())
        var count = customerRepository.countTotalItems()

        delegate.showMessage("You are offline..store to local")
    }

    fun addEngineerWrapperRealm() {
        var engineer = Customer()

        engineer.setItemId("0")
        engineer.name = delegate.getName()
        engineer.email = delegate.getEmail()
        engineer.phone = delegate.getPhone()
        engineer.amount = delegate.getAmount().toFloat()
        engineer.payment_mode = viewModel.payment_mode.toString()
        engineer.created = Date()

        engineer.add = true

        var count1 = customerRepository.countTotalItems()
        customerRepository.insertAddedItem(engineer, engineer.itemId.toString())

        var count = customerRepository.countTotalItems()

        delegate.showMessage("You are offline..store to local")
        delegate.clearData()
    }


    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        val text = s.toString()
        if (text.length > 0) {
            if (text == delegate.getEmail()) {
                delegate.removeEmailError()
            }
            if (text == delegate.getAmount()) {
                delegate.removeAmountError()
            }
            if (text == delegate.getName()) {
                delegate.removeNameError()
            }
        }
    }

    override fun afterTextChanged(s: Editable) {

    }

    fun addEngineerWrapper() {

        if (viewModel.addResponseSingle == null) {
            viewModel.addResponseSingle =
                delegate.getEngineerId()?.let {
                    wrapper.addUser(
                        delegate.getName(),
                        delegate.getEmail(),
                        delegate.getPhone(),
                        delegate.getAmount(),
                        viewModel.payment_mode,
                        it, viewModel.lat.toString(), viewModel.lng.toString()
                    )
                }
        }
        disposable?.dispose()

        disposable = this.viewModel.addResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(loginResponse: AddEngineerResponse) {

                    viewModel.addResponse = loginResponse
                    viewModel.addResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        loginSuccess()
                    } else {
                        loginResponse.message?.let { loginFailed(it) }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.addResponseSingle = null
                    var message: String
                    if (e is UnknownHostException) {
                        message = BaseServiceRxWrapper.NO_INTERNET_CONNECTION
                    } else {
                        message = e.message.toString()
                    }
                    loginFailed(message)
                    dispose()
                }
            })
    }

    private fun loginSuccess() {
        if (viewModel.isEditMode) {
            delegate.showMessage("Edit Successfully")
        } else {
            delegate.showMessage("Added Successfully")
            delegate.clearData()
        }
        delegate.hideProgressBar()
    }

    private fun loginFailed(message: String) {
        delegate.hideProgressBar()
        delegate.showMessage(message)
    }

    fun setEditMode(customer: Customer) {
        viewModel.isEditMode = true
        viewModel.customer = customer
    }

    fun storePaymentMode(i: String) {
        viewModel.payment_mode = PaymentMode.getPaymentMode(i)
    }

    fun editEngineerWrapper() {

        if (viewModel.addResponseSingle == null) {
            viewModel.addResponseSingle =
                delegate.getEngineerId()?.let {
                    wrapper.editUser(
                        viewModel.customer?.itemId.toString(),
                        delegate.getName(),
                        delegate.getEmail(),
                        delegate.getPhone(),
                        delegate.getAmount(),
                        viewModel.payment_mode,
                        it
                    )
                }
        }
        disposable?.dispose()

        disposable = this.viewModel.addResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(loginResponse: AddEngineerResponse) {

                    viewModel.addResponse = loginResponse
                    viewModel.addResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        loginSuccess()
                    } else {
                        loginResponse.message?.let { loginFailed(it) }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.addResponseSingle = null
                    var message: String
                    if (e is UnknownHostException) {
                        message = BaseServiceRxWrapper.NO_INTERNET_CONNECTION
                    } else {
                        message = e.message.toString()
                    }
                    loginFailed(message)
                    dispose()
                }
            })
    }

    fun storeLocation(currentLocation: Location?) {
        viewModel.lat = currentLocation?.latitude?.toFloat()!!
        viewModel.lng = currentLocation.longitude.toFloat()
    }
}
