package com.example.salesengineer.main.home

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatSpinner
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView
import com.example.customermanageadmin.models.FilterType
import com.example.salesengineer.R
import com.example.salesengineer.application.utils.PermissionRequestCodes
import com.example.salesengineer.application.utils.PermissionUtils
import com.example.salesengineer.main.StandardActivity
import com.example.salesengineer.main.customers.CustomerAnalyticResponse
import com.example.salesengineer.main.customers.CustomersListActivity
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.customers.livedata.CustomerViewHolder
import com.example.salesengineer.main.home.livedata.FilterData
import com.example.salesengineer.main.home.livedata.MainPagedListAdapter
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import com.example.salesengineer.main.logout.LogoutActivit
import com.example.salesengineer.main.register.AddUserActivity
import com.example.salesengineer.main.register.PaymentMode
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_home_screen.*
import kotlinx.android.synthetic.main.nav_header.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : StandardActivity(), MainActivityViewDelegate,
    TransactionViewHolder.CategoryClickListener, CustomerViewHolder.CustomerClickListner {
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    override fun onClickEngineer(customer: Customer) {

        AddUserActivity.launch(this, customer)
    }

    var companies_list = PaymentMode.getPaymentModeList(true)
    private lateinit var filterAdapter: FilterAdapter

    private var menu: Menu? = null
    private var presenter: MainActivityPresenter? = null

    private lateinit var layoutManager: LinearLayoutManager
    private var menuItemsAdapter: MainPagedListAdapter? = null

    lateinit var fromdate: EditText
    lateinit var todate: EditText
    lateinit var name: EditText
    lateinit var paymentMode: AppCompatSpinner
    private var dateFormatter: SimpleDateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.US)

    var dialog: Dialog? = null
    var fromDatePickerDialog: DatePickerDialog? = null
    var toDatePickerDialog: DatePickerDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        this.bindViews()

    }

    override fun onResume() {
        super.onResume()
        presenter?.getDetail()
    }

    private fun bindViews() {
        if (presenter == null) {
            this.presenter = customerRealmRepository?.let {
                deliverrSharedPrefs?.let { it1 ->
                    MainActivityPresenter(
                        this,
                        RetrofitRxWrapper(),
                        it, isConnectedToInternet(), it1
                    )
                }
            }
        }

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_drawer_orange)
        supportActionBar?.setTitle("DashBoard")

        navigationView.setNavigationItemSelectedListener(this::onClickNavigationDrawerItem)
        navigationView.getHeaderView(0).name.setText(deliverrSharedPrefs?.engineerName)
        showList()
    }

    override fun showList() {
        if (!this::filterAdapter.isInitialized) {
            this.filterAdapter =
                FilterAdapter(FilterType.getCategoryList(), this, presenter?.getSelectedPosition())
            recyclerView_category.adapter = this.filterAdapter
            recyclerView_category.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

            onClickCategory(FilterType.TODAY, position = 0)
        }
    }

    override fun notifyAdapter() {
        filterAdapter.notifyDataSetChanged()
    }

    override fun getEngineerId(): String {
        return deliverrSharedPrefs?.engineerId.toString()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menuInflater.inflate(R.menu.main_menu, menu)
        this.menu = menu
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                true
            }
            R.id.date_range -> {
                ShowFilter()
                true
            }
            R.id.sync -> {
                showProgressBar()
                syncData()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun syncData() {
        if (isConnectedToInternet()) {
            presenter?.sync()
        } else {
            showMessage("Connect Internet")
            hideProgressBar()
        }
    }

    fun onClickNavigationDrawerItem(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.add_engineer -> {
                AddUserActivity.launch(this)
            }
            R.id.customer_feeds -> {
                CustomersListActivity.launch(this)
            }
            R.id.logout -> {
                LogoutActivit.launch(this)
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun showDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_date_range)

        var okButton = dialog.findViewById<AppCompatButton>(R.id.ok)
        var cancelButton =
            dialog.findViewById<AppCompatButton>(R.id.cancel)
        var calendar_view =
            dialog.findViewById<DateRangeCalendarView>(R.id.calendar_view)

        var nextYear: Calendar = Calendar.getInstance()
        nextYear.add(Calendar.MONTH, 10)
        nextYear.add(Calendar.YEAR, 2019)

        var lastYear: Calendar = Calendar.getInstance()
        lastYear.add(Calendar.MONTH, 10)
        lastYear.add(Calendar.YEAR, 2018)

        okButton.setOnClickListener {
            dialog.dismiss()
        }
        cancelButton.setOnClickListener { dialog.dismiss() }

        var startMonth: Calendar = Calendar.getInstance();
        startMonth.add(Calendar.MONTH, -2);
        var endMonth: Calendar = Calendar.getInstance();
        endMonth.add(Calendar.MONTH, 5)
        calendar_view.setSelectedDateRange(startMonth, startMonth);
        calendar_view.setVisibleMonthRange(startMonth, endMonth);
//        calendar_view.setVisibleMonthRange(lastYear, nextYear);

        calendar_view.setCalendarListener(object : DateRangeCalendarView.CalendarListener {
            override fun onFirstDateSelected(startDate: Calendar) {
                Toast.makeText(
                    this@MainActivity,
                    "Start Date: " + startDate.time.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onDateRangeSelected(startDate: Calendar, endDate: Calendar) {
                Toast.makeText(
                    this@MainActivity,
                    "Start Date: " + startDate.time.toString() + " End date: " + endDate.time.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
        dialog.show()

    }

    companion object {
        fun getLauncherIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }

        @JvmStatic
        fun launch(context: Context) {
            context.startActivity(getLauncherIntent(context))
        }
    }


    private fun ShowFilter() {

        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val menuLayout = inflater.inflate(R.layout.dialog_date_range_filter, null)
        val dg = AlertDialog.Builder(this)
        fromdate = menuLayout.findViewById(R.id.fromdate) as EditText
        todate = menuLayout.findViewById(R.id.todate) as EditText
        name = menuLayout.findViewById(R.id.name) as EditText
        paymentMode = menuLayout.findViewById(R.id.payment_mode) as AppCompatSpinner

        dg.setView(menuLayout)
        dialog = dg.create()

        val c = Calendar.getInstance()
        val c2 = Calendar.getInstance()

        val btnCancel = menuLayout.findViewById(R.id.cancelbtn) as Button
        val btnClear = menuLayout.findViewById(R.id.clearbtn) as Button
        val btnSet = menuLayout.findViewById(R.id.setbtn) as Button

        setSpinner()

        name.setText(presenter?.viewModel?.filteData?.name)
        fromdate.setText(presenter?.viewModel?.filteData?.from_date)
        todate.setText(presenter?.viewModel?.filteData?.to_Date)

        btnCancel.setOnClickListener {
            dialog?.dismiss()
        }

        btnClear.setOnClickListener {
            fromdate.setText("")
            todate.setText("")
            name.setText("")
            paymentMode.setSelection(0)
            presenter?.viewModel?.filteData = FilterData()
            presenter?.applyFilter()
            dialog?.dismiss()
        }

        fromdate.setOnClickListener(View.OnClickListener {
            fromDatePickerDialog?.getDatePicker()?.setMaxDate(System.currentTimeMillis())
            fromDatePickerDialog?.show()
        })

        todate.setOnClickListener(View.OnClickListener {
            toDatePickerDialog?.getDatePicker()?.setMaxDate(System.currentTimeMillis())
            toDatePickerDialog?.show()
        })

        btnSet.setOnClickListener {
            try {
                presenter?.applyFilter()

            } catch (ex: ParseException) {
                ex.printStackTrace()
            }
        }

        try {
            fromDatePickerDialog = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    try {
                        val newDate = Calendar.getInstance()
                        newDate.set(year, monthOfYear, dayOfMonth)

                        presenter?.saveFromDate(newDate);

                        fromdate.setText(dateFormatter?.format(newDate.time))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)
            )

            toDatePickerDialog = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    try {
                        val newDate = Calendar.getInstance()
                        newDate.set(year, monthOfYear, dayOfMonth)

                        presenter?.saveToDate(newDate);

                        todate.setText(dateFormatter?.format(newDate.time))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }, c2.get(Calendar.YEAR), c2.get(Calendar.MONTH), c2.get(Calendar.DAY_OF_MONTH)
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

        dialog?.show()
    }

    private fun setSpinner() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, companies_list)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        paymentMode.setAdapter(adapter)

        if (presenter?.viewModel?.filteData?.paymentMode != 0) {
            for (i in companies_list.indices) {
                if (com.example.customermanageadmin.models.PaymentMode.getPaymentMode(companies_list[i]) == (presenter?.viewModel?.filteData?.paymentMode)) {
                    paymentMode.setSelection(i)
                }
            }
        }

        paymentMode.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {

                presenter?.storePaymentMode(companies_list.get(i))
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {
            }
        })
    }

    override fun dismissDialog() {
        dialog?.dismiss()
    }

    override fun getTodate(): String {
        return todate.text.toString();
    }

    override fun getFromDate(): String {
        return fromdate.text.toString()
    }

    override fun getName(): String {
        return name.text.toString()
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
//        recyclerView_customers.visibility = View.VISIBLE
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
//        recyclerView_customers.visibility = View.GONE
    }

    override fun setAdapter(it: CustomerAnalyticResponse, filterData: FilterData) {
        setListAdapter(it, filterData)
    }

    private fun setListAdapter(
        customerListResponse: CustomerAnalyticResponse,
        filterData: FilterData
    ) {
        val viewModel: MainActivityViewModel = this.getViewModel(MainActivityViewModel::class.java)


        this.menuItemsAdapter =
            MainPagedListAdapter(filterData, customerListResponse, this)

        viewModel.initialLoading.observe(this, androidx.lifecycle.Observer {
            this.presenter?.handleInitialLoadingNetworkState(it)
        })

        viewModel.networkState.observe(this, androidx.lifecycle.Observer {
            this.menuItemsAdapter?.setNetworkState(it)
        })

        viewModel.menuItemsLiveData.observe(this, androidx.lifecycle.Observer {
            this.menuItemsAdapter?.submitList(it)
        })

        this.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL,
            false
        )

        recyclerView_customers.layoutManager = this.layoutManager
        recyclerView_customers.adapter = this.menuItemsAdapter
    }

    override fun onClickCategory(engineer: String, position: Int) {

        presenter?.storeSelectedPosition(position)

        when (engineer) {
            FilterType.TODAY -> {
                presenter?.setToday()
            }
            FilterType.YESTERDAY -> {
                presenter?.setYesterDay()
            }
            FilterType.THIS_WEEK -> {
                presenter?.setWeek()
            }
            FilterType.CUSTOM -> {
                presenter?.customFilter()
            }
        }
        presenter?.getDetail()
    }

    override fun getLocation() {
        if (this.handleLocationPermissions()) {
            mFusedLocationClient?.lastLocation?.addOnSuccessListener { location : Location? ->
                // Got last known location. In some rare situations this can be null.
                updateUI(location)
            }

        }
    }

    private fun updateUI( location : Location?) {
        presenter?.storeLocation(location)
        presenter?.sendAddedCustomers()
    }


    private fun handleLocationPermissions(): Boolean {
        if (!PermissionUtils.areLocationServicesEnabled(this)) {
            if (!presenter?.locationServiceDialogShowing!!) {       //condition to prevent overlap of location service dialog
                presenter?.locationServiceDialogShowing = true
                locationManager?.showEnableLocationDialog(this,
                    LogoutActivit.REQUEST_LOCATION_SERVICE
                )
            }
            return false
        } else {
            //Location Services are enabled
            //Permission Disabled
            if (!PermissionUtils.isLocationPermissionAndServiceGranted(this)) {
                if (PermissionUtils.checkIfUserHasDeniedLocationPermissionToNever(
                        deliverrSharedPrefs,
                        this
                    )
                ) {
                    //show SnackBar to open permissions settings
                    showMessage(R.string.enable_location_permission_deliveryAddress_snackbar_label)
                    return false
                }
                if (PermissionUtils.shouldShowLocationPermissionRationale(this)) {
                    //show SnackBar to again popup permissions dialog
                    showMessage(R.string.enable_location_permission_deliveryAddress_snackbar_label)
                } else {
                    //asking for permission first time
                    presenter?.locationPermissionDialogShowing = true
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        PermissionRequestCodes.REQUEST_LOCATION
                    )
                }

                return false
            } else {
                //Location services enabled
                //Permission enabled
                return true
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LogoutActivit.REQUEST_LOCATION_SERVICE) {
            this.presenter?.locationServiceDialogShowing = false

            if (resultCode == Activity.RESULT_OK) {
                //location service enabled > check for permission
                this.getLocation()
            } else {
                //location service disabled > show snackBar
                if (!PermissionUtils.areLocationServicesEnabled(this)) {
                    showMessage(R.string.msg_request_location_service)
                }
            }
        }
    }

}
