package com.example.salesengineer.main.register

import androidx.lifecycle.ViewModel
import com.example.salesengineer.main.customers.Engineer
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.register.rx.AddEngineerResponse
import io.reactivex.Single

class AddUserViewModel : ViewModel() {

    var payment_mode = PaymentMode.getPaymentMode(PaymentMode.OTHER)
    var addResponseSingle: Single<AddEngineerResponse>? = null
    var addResponse: AddEngineerResponse? = null
    var customer: Customer?=null
    var isEditMode: Boolean =false
    var lat: Float=0.0f;
    var lng: Float=0.0f;

}
