package com.example.customermanageadmin.main.splash

import androidx.lifecycle.ViewModel
import com.example.salesengineer.main.customers.livedata.CustomerListResponse
import com.example.salesengineer.main.register.rx.AddEngineerResponse
import io.reactivex.Single

class SplashViewModel : ViewModel() {

    var customerListSingle: Single<CustomerListResponse>? = null
    var customerListResponse: CustomerListResponse ?=null

    var lat: Float=0.0f;
    var lng: Float=0.0f;

    var editResponseSingle: Single<AddEngineerResponse>? = null
    var editResponse: AddEngineerResponse?=null

    var addResponseSingle: Single<AddEngineerResponse>? = null
    var addResponse: AddEngineerResponse ?=null
}
