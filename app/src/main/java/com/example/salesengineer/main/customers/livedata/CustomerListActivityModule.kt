package com.example.salesengineer.main.customers.livedata

import androidx.lifecycle.LifecycleOwner
import com.example.salesengineer.main.customers.CustomersListActivity
import dagger.Module
import dagger.Provides
import deliverr.deliverrconsumer.di.scopes.ActivityScope
import deliverr.deliverrconsumer.referral.account_status.CustomerListViewDelegate

@Module
class CustomerListActivityModule {
    @Provides
    @ActivityScope
    fun provideBrandItemsActivityViewDelegate(activity: CustomersListActivity): CustomerListViewDelegate =
        activity

    @Provides
    @ActivityScope
    fun provideLifecycleOwner(activity: CustomersListActivity): LifecycleOwner = activity
}
