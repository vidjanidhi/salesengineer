package com.example.salesengineer.main.customers.livedata

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.salesengineer.R
import com.example.salesengineer.application.utils.NetworkState
import com.example.salesengineer.main.customers.CustomerAnalyticResponse

private const val LAYOUT_TYPE_ITEM = 0
private const val LAYOUT_TYPE_ITEM_HEADER = 1

class CustomerPagedListAdapter(
    val customerListResponse: CustomerAnalyticResponse,
    private val clickListener: CustomerViewHolder.CustomerClickListner
) : PagedListAdapter<Customer, RecyclerView.ViewHolder>(
    Customer.DIFF_CALLBACK) {

    private var networkState: NetworkState? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return when (viewType) {

            LAYOUT_TYPE_ITEM_HEADER -> {
                val itemView = layoutInflater.inflate(
                    R.layout.list_item_sales_feeds_header,
                    parent,
                    false
                )
                CustomerHeaderViewHolder(itemView)
            }
            LAYOUT_TYPE_ITEM -> {
                val itemView = layoutInflater.inflate(
                    R.layout.list_item_sales_feeds,
                    parent,
                    false
                )
                CustomerViewHolder(itemView,clickListener)
            }

            else -> {
                val itemView = layoutInflater.inflate(
                    R.layout.list_item_sales_feeds,
                    parent,
                    false
                )
                SpaceViewHolder(itemView)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CustomerViewHolder -> {
                val item = this.getItem(position-1)
                if (item != null) {
                    item?.let { holder.bind(it) }
                }
            }
            is CustomerHeaderViewHolder ->{
                if (customerListResponse != null) {
                    customerListResponse?.let { holder.bind(it) }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when(position){
             0 -> LAYOUT_TYPE_ITEM_HEADER
            else -> LAYOUT_TYPE_ITEM
        }

    }

    override fun getItemCount(): Int {
        return super.getItemCount()+1
    }
    fun setNetworkState(newState: NetworkState) {
        this.networkState = newState
        this.notifyDataSetChanged()
    }


}
