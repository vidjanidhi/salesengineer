package com.example.customermanageadmin.main.splash

import android.location.Location
import com.example.customermanageadmin.realm_model.CustomerRepository
import com.example.customermanageadmin.realm_model.EngineerRepository
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.customers.livedata.CustomerListResponse
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import com.example.salesengineer.main.register.rx.AddEngineerResponse
import com.example.salesengineer.prefs.SharedPrefs
import com.example.salesengineer.prefs.SharedPrefsService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class SplashActivityPresenter(
    private val delegate: SplashDelegate,
    private val wrapper: RetrofitRxWrapper,
    private val customerRepository: CustomerRepository,
    private val sharedPrefs: SharedPrefsService
) {
    internal var locationServiceDialogShowing: Boolean = false
    internal var locationPermissionDialogShowing: Boolean = false
    private var disposable: Disposable? = null

    private val viewModel: SplashViewModel by lazy {
        this.delegate.getViewModel(SplashViewModel::class.java)
    }
fun initialize(){
    sendAddedCustomers()
}
    fun getCustomerList() {
        if (viewModel.customerListSingle == null) {
            viewModel.customerListSingle =

                wrapper.getBrandItemListCall(sharedPrefs.engineerId)

        }
        disposable?.dispose()

        disposable = this.viewModel.customerListSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<CustomerListResponse>() {
                override fun onSuccess(customerListResponse: CustomerListResponse) {

                    viewModel.customerListResponse = customerListResponse
                    viewModel.customerListSingle = null
                    if (customerListResponse.isSuccess == true) {
                        storeToLocal()
                    } else {
                        delegate.openNext()
//                        sendAddedCustomers()
                        customerListResponse.message?.let { }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.customerListSingle = null
                    dispose()
                }
            })
    }

    fun storeToLocal() {
        customerRepository.deleteAllItems()
        var count = customerRepository.countTotalItems()

        viewModel.customerListResponse?.payload?.let {
            customerRepository.insertOrUpdate(it)
        }
        var count1 = customerRepository.countTotalItems()
        delegate.openNext()

//        delegate.openNext()
    }

     fun sendAddedCustomers() {
        var engineers = customerRepository.findAdds()
        if (engineers.size > 0) {
            if (viewModel.lat == 0.0f && viewModel.lng == 0.0f) {
                delegate.getLocation()
            } else {
                addCustomerWrapper(engineers[0])
            }
        } else {
            sendEditedCustomers()
        }

    }

    fun addCustomerWrapper(customer: Customer) {

        if (viewModel.addResponseSingle == null) {
            viewModel.addResponseSingle =
                wrapper.addUser(
                    customer.getName(),
                    customer.getEmail(),
                    customer.getPhone(),
                    customer.amount.toString(),
                    customer.payment_mode.toInt(),
                    sharedPrefs.engineerId,
                    viewModel.lat.toString(),
                    viewModel.lng.toString()
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.addResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(loginResponse: AddEngineerResponse) {

                    viewModel.addResponse = loginResponse
                    viewModel.addResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        deleteAddedEngineer(customer.email)
                        sendAddedCustomers()
                    } else {
                        deleteAddedEngineer(customer.email)
                        sendAddedCustomers()
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.addResponseSingle = null
                    dispose()
                }
            })
    }

    private fun deleteAddedEngineer(email: String) {
        customerRepository.deleteAddedItem(email)
    }

    private fun sendEditedCustomers() {
        var engineers = customerRepository.findEdits()
        if (engineers.size > 0) {
            editCustomerWrapper(engineers[0])
        } else {
            success()
        }
    }

    fun editCustomerWrapper(customer: Customer) {

        if (viewModel.editResponseSingle == null) {
            viewModel.editResponseSingle =
                wrapper.editUser(
                    customer?.itemId.toString(),
                    customer.getName(),
                    customer.getEmail(),
                    customer.getPhone(),
                    customer.amount.toString(),
                    customer.payment_mode.toInt(),
                    sharedPrefs.engineerId
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.editResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(editResponse: AddEngineerResponse) {

                    viewModel.editResponse = editResponse
                    viewModel.editResponseSingle = null
                    if (editResponse.isSuccess == true) {
                        deleteEditedEngineer(customer.itemId.toString())
                    } else {
                        deleteEditedEngineer(customer.itemId.toString())
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.editResponseSingle = null
                    dispose()
                }
            })
    }

    private fun deleteEditedEngineer(id: String) {
        customerRepository.deleteEditedItem(id)
        sendEditedCustomers()
    }

    private fun success() {
        getCustomerList()

    }
    fun storeLocation(currentLocation: Location?) {
        viewModel.lat = currentLocation?.latitude?.toFloat()!!
        viewModel.lng = currentLocation.longitude.toFloat()
    }
}
