package com.example.customermanageadmin.main.splash

import androidx.lifecycle.ViewModel

interface SplashDelegate {
    fun <T : ViewModel> getViewModel(clazz: Class<T>): T
    fun openNext()
    fun getLocation()
}
