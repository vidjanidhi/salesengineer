package com.example.salesengineer.main.home

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import com.example.salesengineer.main.customers.CustomerAnalyticResponse
import com.example.salesengineer.main.home.livedata.FilterData

interface MainActivityViewDelegate {
    fun showMessage(@StringRes stringId: Int)
    fun showMessage(message: String)

    fun isConnectedToInternet(): Boolean
    fun <T : ViewModel> getViewModel(clazz: Class<T>): T

    fun getTodate(): String
    fun getFromDate(): String
    fun getName(): String
    fun hideProgressBar()
    fun showProgressBar()
    fun setAdapter(it: CustomerAnalyticResponse,filterData: FilterData)
    fun dismissDialog()
    fun getEngineerId():String

    fun showList()
    fun notifyAdapter()
    fun getLocation()

}
