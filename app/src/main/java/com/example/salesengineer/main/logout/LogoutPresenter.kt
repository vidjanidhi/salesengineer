package com.example.customermanageadmin.main.logout

import android.location.Location
import com.example.customermanageadmin.realm_model.CustomerRepository
import com.example.customermanageadmin.realm_model.RealmManager
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import com.example.salesengineer.main.register.rx.AddEngineerResponse
import com.example.salesengineer.prefs.SharedPrefsService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers


class LogoutPresenter(
    private val delegate: LogoutDelegate,
    private val customerRepository: CustomerRepository,
    private val wrapper: RetrofitRxWrapper,
    private val sharedPrefs: SharedPrefsService
) {
    internal var locationServiceDialogShowing: Boolean = false
    internal var locationPermissionDialogShowing: Boolean = false
    private var disposable: Disposable? = null

    private val viewModel: LogoutViewModel by lazy {
        this.delegate.getViewModel(LogoutViewModel::class.java)
    }

    fun initialize() {
        if (RealmManager(customerRepository).checkIfNeedsSync()) {
            syncData()
        } else {
            deleteAllData()
        }
    }

    fun syncData() {
        if (delegate.isConnectedToInternet()) {
            syncWrapper()
        } else {
            delegate.showErrorMessage("Please Connect to internet to sync data")
            delegate.finishCurrent()
        }
    }

    private fun syncWrapper() {
        sendAddedCustomers()
    }

    fun sendAddedCustomers() {
        var engineers = customerRepository.findAdds()
        if (engineers.size > 0) {
            if (viewModel.lat == 0.0f && viewModel.lng == 0.0f) {
                delegate.getLocation()
            } else {
                addCustomerWrapper(engineers[0])
            }
        } else {
            sendEditedCustomers()
        }

    }


    fun addCustomerWrapper(customer: Customer) {

        if (viewModel.addResponseSingle == null) {
            viewModel.addResponseSingle =
                wrapper.addUser(
                    customer.getName(),
                    customer.getEmail(),
                    customer.getPhone(),
                    customer.amount.toString(),
                    customer.payment_mode.toInt(),
                    sharedPrefs.engineerId,
                    viewModel.lat.toString(),
                    viewModel.lng.toString()
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.addResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(loginResponse: AddEngineerResponse) {

                    viewModel.addResponse = loginResponse
                    viewModel.addResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        deleteAddedEngineer(customer.email)
                        sendAddedCustomers()
                    } else {
                        deleteAddedEngineer(customer.email)
                        sendAddedCustomers()
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.addResponseSingle = null
                    dispose()
                }
            })
    }

    private fun deleteAddedEngineer(email: String) {
        customerRepository.deleteAddedItem(email)
    }

    private fun sendEditedCustomers() {
        var engineers = customerRepository.findEdits()
        if (engineers.size > 0) {
            editCustomerWrapper(engineers[0])
        } else {
            deleteAllData()
        }
    }

    fun editCustomerWrapper(customer: Customer) {

        if (viewModel.editResponseSingle == null) {
            viewModel.editResponseSingle =
                wrapper.editUser(
                    customer?.itemId.toString(),
                    customer.getName(),
                    customer.getEmail(),
                    customer.getPhone(),
                    customer.amount.toString(),
                    customer.payment_mode.toInt(),
                    sharedPrefs.engineerId
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.editResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(editResponse: AddEngineerResponse) {

                    viewModel.editResponse = editResponse
                    viewModel.editResponseSingle = null
                    if (editResponse.isSuccess == true) {
                        deleteEditedEngineer(customer.itemId.toString())
                    } else {
                        deleteEditedEngineer(customer.itemId.toString())
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.editResponseSingle = null
                    dispose()
                }
            })
    }

    private fun deleteEditedEngineer(id: String) {
        customerRepository.deleteEditedItem(id)
        sendEditedCustomers()
    }

    fun deleteAllData() {
        deleteRealm()
        deletePreferences()
        delegate.showSignInScreen()
    }

    private fun deletePreferences() {
        delegate.clearPreferences()
    }

    fun deleteRealm() {
        customerRepository.deleteAllItems()
    }

    fun storeLocation(currentLocation: Location?) {
        viewModel.lat = currentLocation?.latitude?.toFloat()!!
        viewModel.lng = currentLocation.longitude.toFloat()
    }


}
