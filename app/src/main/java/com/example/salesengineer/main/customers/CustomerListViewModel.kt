package com.example.salesengineer.main.customers

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.salesengineer.application.utils.NetworkState
import com.example.salesengineer.main.customers.livedata.Customer
import com.example.salesengineer.main.login.rx.RetrofitRxWrapper
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.CustomerListDataSource
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.CustomerListDataSourceFactory
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.FUTURE_PAGE_SIZE
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.INITIAL_PAGE_SIZE
import io.reactivex.Single

class CustomerListViewModel : ViewModel() {

    var customerListSingle: Single<CustomerAnalyticResponse>? = null
    var customerListResponse: CustomerAnalyticResponse? = null

    lateinit var initialLoading: LiveData<NetworkState>
    lateinit var networkState: LiveData<NetworkState>
    lateinit var menuItemsLiveData: LiveData<PagedList<Customer>>

    lateinit var brandItemsDataSource: LiveData<CustomerListDataSource>

    fun retry() {
        this.brandItemsDataSource.value!!.retryLoading()
    }

    fun initItems(wrapper: RetrofitRxWrapper, engineerId: String,
                  isNetworkAvailable: Boolean) {
        val brandItemsDataSourceFactory = CustomerListDataSourceFactory(
            wrapper, engineerId,isNetworkAvailable
        )
        this.brandItemsDataSource = brandItemsDataSourceFactory.brandItemsDataSourceLiveData

        this.initialLoading = Transformations.switchMap(
            brandItemsDataSourceFactory.brandItemsDataSourceLiveData
        ) { it.initialLoading }
        this.networkState = Transformations.switchMap(
            brandItemsDataSourceFactory.brandItemsDataSourceLiveData
        ) { it.networkState }

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(INITIAL_PAGE_SIZE)
            .setPageSize(FUTURE_PAGE_SIZE)
            .setPrefetchDistance(FUTURE_PAGE_SIZE)
            .build()

        this.menuItemsLiveData = LivePagedListBuilder(brandItemsDataSourceFactory, pagedListConfig)
            .build()
    }
}
