package com.example.salesengineer.main.register

import androidx.lifecycle.ViewModel

internal interface AddUserDelegate {

    fun  getEngineerId(): String?

    fun isConnectedToInternet(): Boolean

    fun getEmail(): String
    fun getName(): String
    fun getPhone(): String


    fun showMessage(stringMessageid: Int)
    fun showMessage(stringMessage: String)

    fun finishActivity()
    fun getAmount(): String

    fun hideKeyboard()

    fun removeEmailError()

    fun removeAmountError()


    fun <T : ViewModel> getViewModel(clazz: Class<T>): T
    fun hideProgressBar()
    fun showProgressBar()
    fun removeNameError()
    fun clearData()
    fun getLocation()

}
