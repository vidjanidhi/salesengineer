@file:JvmName("PlayServicesUtils")

package com.example.salesengineer.application.utils

import android.app.Activity
import android.content.IntentSender
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability

/*
 * Define a request code to send to Google Play services
 * This code is returned in Activity.onActivityResult
 */
private const val CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000

fun onConnectionFailed(connectionResult: ConnectionResult, activity: Activity) {
    // Start an Activity that tries to resolve the error
    if (connectionResult.hasResolution()) {
        try {
            connectionResult.startResolutionForResult(activity, CONNECTION_FAILURE_RESOLUTION_REQUEST)
        } catch (e: IntentSender.SendIntentException) {
            e.printStackTrace()
        }

    } else {
        // If no resolution is available, display a dialog to the user with the error.
        showPlayServicesErrorDialog(connectionResult.errorCode, activity)
    }
}

/**
 * Show appropriate play services error dialog
 * TODO: May want to append to a fragment
 *
 * @param code
 * @param activity
 */
private fun showPlayServicesErrorDialog(code: Int?, activity: Activity) {
    val dialog = GoogleApiAvailability.getInstance().getErrorDialog(
            activity,
            code!!,
            CONNECTION_FAILURE_RESOLUTION_REQUEST
    )
    dialog.setOnCancelListener { activity.finish() }
    dialog.show()
}
