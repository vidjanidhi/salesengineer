package com.example.salesengineer.application.utils

import android.view.View
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.example.salesengineer.R

class ErrorViewHolder(
        private val errorView: View,
        private val retryListener: RetryListener
) : RecyclerView.ViewHolder(errorView) {
    private val retryButton: AppCompatButton = this.errorView.findViewById(R.id.retryButton)

    init {
        this.retryButton.setOnClickListener {
            this.retryListener.retry()
        }
    }
}

interface RetryListener {
    fun retry()
}
