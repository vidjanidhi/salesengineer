package com.example.customermanageadmin.models

import com.example.customermanageadmin.models.PaymentMode.Companion.PAYPAL
import com.example.customermanageadmin.models.PaymentMode.Companion.PAYTM
import com.example.customermanageadmin.models.PaymentMode.Companion.STRIPE

class FilterType(
     val name:String,
     val boolean: Boolean
) {

    companion object {
        const val TODAY = "TODAY"
        const val YESTERDAY = "YESTERDAY"
        const val THIS_WEEK = "THIS WEEK"
        const val CUSTOM = "CUSTOM"

        fun getPaymentMode(mode: String): Int {
            if (mode.equals(TODAY)) {
                return 1;
            } else if (mode.equals(YESTERDAY)) {
                return 2;
            } else if (mode.equals(THIS_WEEK)) {
                return 3;
            } else if (mode.equals(CUSTOM)) {
                return 4;
            } else {
                return 0;
            }
        }

        fun getCategoryList(): ArrayList<String> {
            var companies_list = ArrayList<String>()


            companies_list.add(TODAY)
            companies_list.add(YESTERDAY)
            companies_list.add(THIS_WEEK)
            companies_list.add(CUSTOM)

            return companies_list
        }

        fun getStringPaymentMode(mode: Int): String {
            return when (mode) {
                1 -> TODAY
                2 -> YESTERDAY
                3 -> THIS_WEEK
                4 -> CUSTOM
                else -> CUSTOM

            }
        }
    }

}
